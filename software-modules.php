<?php /* Template Name: Page | Software Modules */ ?>

<?php get_header(); ?>
<?php

$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$meta  =get_fields($post_id);
$lang_label= "en";
$lang_ext = "" ;
$parent_link = get_page_link( '60');
$parent_id = 60;
$home_label= "Home";

if(pll_current_language() == 'ar'){
    $home_label= "الصفحة الرئيسية";
    $lang_label = pll_current_language();
    $lang_ext = "_ar";
    $parent_link = get_page_link( '1160');
    $parent_id = 1160;
}elseif (pll_current_language() == 'de'){
    $home_label = "Startseite";
    $lang_label = pll_current_language();
    $lang_ext = "_de";
    $parent_link = get_page_link( '1162');
    $parent_id = 1162;
}


?>

<div class="menu-spacer"></div>

<div class="software-modules-page page-top">

    <div class="product-breadcrumbs">
        <div class="bread-inner col-md-10 section-inner">
            <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
            <label class="py-2"> / </label>
            <a href="<?php echo  $parent_link?>" class="list-item py-2">Products</a>
            <label class="py-2"> / </label>
            <label class="py-2"><?php echo $meta['label'] ?></label>
        </div>
    </div>

    <div class="modules section-100-100 ">
        <div class="section-inner col-md-10 flex-column">
            <div class="lg-label page-label-software" data-aos="fade-up" data-aos-delay="100" data-aos-delay="-150"><?php echo $meta['label'] ?></div>

            <?php
                $modules =get_posts( array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'category_name' => 'software_modules'.$lang_ext,
                    'posts_per_page'=>-1,
                    'lang'=>$lang_label
                ));

            if( $modules){ ?>

                <div data-aos="fade-up" data-aos-delay="100" data-aos-delay="-150" class="module-nav-carousel owl-carousel carousel"  data-carousel-items="7" data-carousel-autoplay="false" data-carousel-loop="false" data-carousel-nav="false" data-carousel-dots="false" data-carousel-nav-triggers="false" data-carousel-nav-next-id="" data-carousel-nav-prev-id="" data-autoplay-timeout="6000">
                    <?php
                    $i=0;
                    foreach ( $modules as $module){
                        $i++;
                        ?>
                        <div class="category-card <?php if(isset($_GET['module']) && $_GET['module']==$module->ID){ echo 'active';}else if( !isset($_GET['module']) && $i==1 ){ echo 'active';} ?>" onclick="changeQueryParameter('module','<?php echo $module->ID ?>',null)">
                            <div class="c-label"><?php echo $module->label; ?></div>
                        </div>
                    <?php } ?>
                </div>

            <?php } ?>

            <?php if(isset($_GET['module']) && $_GET['module'] != '' ){?>

                <div class="products-container d-flex justify-content-between">
                    <div class="col-md-3 block-left">

                        <div class="module-card" data-aos="fade-up" data-aos-delay="100">
                            <?php
                                $module  = get_fields($_GET['module']);
                                if($module){
                            ?>

                                    <div class="sm-label t-align"><?php echo $module['label'] ?></div>

                                    <div class="description"><?php echo $module['description'] ?></div>

                                    <img src="<?php echo $module['image']['url'] ?>" alt="<?php echo $module['label'] ?>">

                                <?php } ?>

                        </div>

                    </div>

                    <div class="col-md-8 block-right">
                        <div class="products-row inner">
                            <?php
                                if(isset($_GET['module'])){
                                    $filterModule = $_GET['module'];
                                }else{
                                    $filterModule = null ;
                                }
                                $products = get_pages( array(
                                    'child_of'    => $parent_id,
                                    'meta_key' => 'intervention',
                                    'meta_value' => 'Interventions',
                                ));

                                $i = 0;
                            if($products){
                                foreach ($products as $product){
                                $i++;
                                if(($filterModule==null) || ( $filterModule != null && $product->category == $filterModule)){
                            ?>
                            <a href="<?php echo get_permalink( $product->ID ) ?>" class="product-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                                <div class="product-image">
                                    <?php $image_src = wp_get_attachment_image_src($product->thumbnail_image,'large'); ?>
                                    <div class="image cover ratio-4-3" style="background-image: url('<?php echo $image_src[0] ?>')"></div>

                                </div>
                                <div class="product-info">
                                    <div class="product-label"><?php echo $product->label ?></div>
                                    <div class="product-sublabel"><?php echo $product->sublabel ?></div>
                                </div>

                            </a>

                            <?php }}}?>
                        </div>
                    </div>
                </div>


            <?php } ?>

        </div>
    </div>



</div>

<?php get_footer(); ?>


