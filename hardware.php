<?php /* Template Name: Page | Hardware */ ?>

<?php get_header(); ?>
<?php
//$meta = get_fields('58');

$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$meta  =get_fields($post_id);

$home_label= "Home";
$lang_label= "en";
$lang_ext = "" ;

if(pll_current_language() == 'ar'){
    $home_label= "الصفحة الرئيسية";
    $lang_label = pll_current_language();
    $lang_ext = "_ar";
}elseif (pll_current_language() == 'de'){
    $home_label = "Startseite";
    $lang_label = pll_current_language();
    $lang_ext = "_de";
}
?>

<div class="menu-spacer"></div>
<div class="hardware-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image cover parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $meta['image']; ?>" ></div>
            <div class="banner-info col-12 col-md-10">
                <div class="banner-label"><?php echo  $meta['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $meta['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

	<a id="hardware-systems" class="a-name"></a>
    <?php  if(count($meta['block_information'])>0){?>
        <div class="display-block gray-block">
            <div class="display-block-inner col-md-10">
                <div class="block-label" data-aos="fade-up" data-aos-delay="100"><?php echo  $meta['block_information'][0]['label']; ?></div>
                <div class="block-info">
                    <div class="block-description" data-aos="fade-up" data-aos-delay="100"><?php echo  nl2br($meta['block_information'][0]['description']); ?></div>
                    <div class="block-visual" data-aos="fade-up" data-aos-delay="100">
                        <div class="image ratio-5-3 cover" style="background-image:url('<?php echo  $meta['block_information'][0]['image'] ?>') "></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="grid-display section-80-80 ">
        <div class="section-inner col-md-10 flex-column">
            <div class="grid-inner">
                <?php $i=0; foreach ($meta['grid_information']AS $value){ $i++; ?>
                    <div class="grid-card <?php if($i%2){ echo 'even' ;}else{ echo 'odd'; }?>" >
                        <div class="grid-virtual"<?php if($i%2){?> data-aos="fade-up" data-aos-delay="<?php echo 200+$i*50; ?>"  <?php }else{?> data-aos="fade-up" data-aos-delay="<?php echo 150+$i*50; ?>" <?php } ?>>
                            <div class="line <?php if($i%2){}else{ echo 'margin-left-40'; }?>"></div>
                            <div class="image ratio-5-3 cover" style="background-image: url('<?php echo  $value['image'] ?>')"></div>
                        </div>
                        <div class="grid-info <?php if($i%2){ echo 'padding-left-140';}else{ echo 'padding-right-140'; }?>" <?php if($i%2){?> data-aos="fade-up" data-aos-delay="<?php echo 200+$i*50; ?>"  <?php }else{?> data-aos="fade-up" data-aos-delay="<?php echo 150+$i*50; ?>" <?php } ?>>
                            <div class="sm-label"><?php echo $value['label'] ?></div>
                            <div class="desc op-45"><?php echo nl2br($value['description']) ?></div>

                            <?php if($value['page_link']){
                                $Page_ID =url_to_postid($value['page_link'] );
                                $PostInfo = get_post($Page_ID);
                                ?>
                                <a href="<?php echo $value['page_link'] ?>" class="c-button">
                                    <div class="btn-text"><?php echo $PostInfo->label?></div>
                                </a>
                            <?php } ?>

                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>

    <a name="section1" class="a-name"></a>
    <div class="Filters" data-aos="fade-up" >
        <div class="section-inner col-md-10">
            <div class=" date-filter filter-inner">
                <select class="browser-default" onchange="changeQueryParameter('ProductDate',$(this).val(),'section1')">
                    <option value="" disabled <?php if (!isset($_GET["ProductDate"])){?> selected  <?php  } ?>><?php if(isset($meta['sort_products_by_label'])) echo $meta['sort_products_by_label'] ?></option>
                    <option value="DESC" <?php if (isset($_GET["ProductDate"]) && $_GET["ProductDate"] == "DESC"){?> selected  <?php  } ?>><?php if(isset($meta['oldest_to_newest'])) echo $meta['oldest_to_newest'] ?></option>
                    <option value="ASC" <?php if (isset($_GET["ProductDate"]) && $_GET["ProductDate"] == "ASC"){?> selected  <?php  } ?>><?php if(isset($meta['newest_to_oldest'])) echo $meta['newest_to_oldest'] ?></option>
                </select>
            </div>

        </div>
    </div>

    <div class="hardware-product section-50-50 ">
        <div class="section-inner col-md-10">
            <div class="products-row">
            <?php
            if(isset($_GET['ProductDate'])){
                $filterDate = $_GET['ProductDate'];
            }else{
                $filterDate = "ASC";
            }

            $products = get_pages( array(
                    'child_of'    => $post_id,
                    'sort_order' =>  $filterDate,
                    'meta_key' => 'category',
                    'meta_value' => null,
                ));

            $i = 0;
            foreach ($products as $product){
            $i++;
            ?>
                <a href="<?php echo get_permalink( $product->ID ) ?>" class="product-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                    <div class="product-image">
                            <?php $image_src = wp_get_attachment_image_src($product->thumbnail_image,'large'); ?>
                            <div class="image cover ratio-4-3" style="background-image: url('<?php echo $image_src[0] ?>')"></div>

                    </div>
                    <div class="product-info">
                        <div class="product-label"><?php echo $product->label ?></div>
                        <div class="product-sublabel"><?php echo $product->sublabel ?></div>
                    </div>

                </a>
            <?php } ?>
            </div>

        </div>
    </div>

	<a id="hardware-additional-devices" class="a-name"></a>
    <?php  if(count($meta['block_information'])>1){?>
        <div class=" display-block gradient-block">
            <div class="display-block-inner col-md-10">
                <div class="block-label" data-aos="fade-up" data-aos-delay="100"><?php echo  $meta['block_information'][1]['label']; ?></div>
                <div class="block-info">
                    <div class="block-description " data-aos="fade-up" data-aos-delay="100"><?php echo  nl2br($meta['block_information'][1]['description']); ?></div>
                    <div class="block-visual" data-aos="fade-up" data-aos-delay="100">
                        <div class="image ratio-5-3 cover" style="background-image:url('<?php echo  $meta['block_information'][1]['image'] ?>') "></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <a name="section2" class="a-name"></a>
    <div class="Filters" data-aos="fade-up">
        <div class="section-inner col-md-10">
            <div class=" date-filter filter-inner">
                <select class="browser-default" onchange="changeQueryParameter('AdditionalProductDate',$(this).val(),'section2')">
                    <option value="" disabled <?php if (!isset($_GET["AdditionalProductDate"])){?> selected  <?php  } ?>><?php if(isset($meta['sort_products_by_label'])) echo $meta['sort_products_by_label'] ?></option>
                    <option value="DESC" <?php if (isset($_GET["AdditionalProductDate"]) && $_GET["AdditionalProductDate"] == "DESC"){?> selected  <?php  } ?>><?php if(isset($meta['oldest_to_newest'])) echo $meta['oldest_to_newest'] ?></option>
                    <option value="ASC" <?php if (isset($_GET["AdditionalProductDate"]) && $_GET["AdditionalProductDate"] == "ASC"){?> selected  <?php  } ?>><?php if(isset($meta['newest_to_oldest'])) echo $meta['newest_to_oldest'] ?></option>
                </select>
            </div>

        </div>
    </div>

    <div class="additional-hardware-product section-50-50 ">
        <div class="section-inner col-md-10 flex-column">
            <div class="hardware-rows">
                <?php
                $devices =get_posts( array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'category_name' => 'hardware_devices'.$lang_ext,
                    'posts_per_page'=>-1,
                    'lang'=>$lang_label
                ));

                if($devices){
                foreach ($devices as $device){
                ?>
                    <div class="device-item">
                        <div class="sm-label" data-aos="fade-up"><?php echo $device->label ?></div>
                        <div class="device-desc op-45" data-aos="fade-up"><?php echo nl2br($device->description) ?></div>

                        <div class="products-row">
                            <?php

                            if(isset($_GET['AdditionalProductDate'])){
                                $filterDate = $_GET['AdditionalProductDate'];
                            }else{
                                $filterDate = "ASC";
                            }
                            $additional_products = get_pages( array(
                                'child_of'    => $post_id,
                                'sort_order' =>  $filterDate,
                            ));

                            $i = 0;

                            if($additional_products){
                            foreach ($additional_products as $product){
                                if($product->category != null){
                                    if($product->category == $device->ID){
                                        $i++
                            ?>
                                <a href="<?php echo get_permalink( $product->ID ) ?>" class="product-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                                    <div class="product-image">
                                        <?php $image_src = wp_get_attachment_image_src($product->thumbnail_image,'large'); ?>
                                        <div class="image cover ratio-4-3" style="background-image: url('<?php echo $image_src[0] ?>')"></div>

                                    </div>
                                    <div class="product-info">
                                        <div class="product-label"><?php echo $product->label ?></div>
                                        <div class="product-sublabel"><?php echo $product->sublabel ?></div>
                                    </div>

                                </a>
                            <?php }}}}?>
                        </div>
                    </div>
                <?php }} ?>

            </div>
        </div>
    </div>

</div>


<?php get_footer(); ?>

