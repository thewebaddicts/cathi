$(document).ready(function(){
    window.addEventListener('scroll', InitializeMenuScroll);
    function InitializeMenuScroll(){
        var scroll_start = 0;
        if (($(document).scrollTop() - 40) > 0){
            $('header.main_menu').addClass('compact');
            $('.page-top').addClass('compact');
        }else{
            $('header.main_menu').removeClass('compact');
            $('.page-top').removeClass('compact');
        }
    }

    if($(".banner-carousel").length>0) {
        var owl2 = $(".banner-carousel")
        owl2.owlCarousel({
            items:1,
            autoWidth:true,
            autoplay: true,
            slideTransition: 'linear',
            autoplayTimeout: 6000,
            autoplaySpeed: 2000,
            autoplayHoverPause: false,
            loop: true,
            nav: false,
            dots:false,
            animateOut: 'fadeOut',
        });
    }

    if($(".module-nav-carousel").length>0) {
        var owl5 = $(".module-nav-carousel")
        owl5.owlCarousel({
            items: parseInt(owl5.attr("data-carousel-items")),
            nav: owl5
                .attr("data-carousel-nav")
                .toLowerCase() === "true"
                ? true
                : false,
            dots: true,
            autoplay:
                owl5
                    .attr("data-carousel-autoplay")
                    .toLowerCase() === "true"
                    ? true
                    : false,
            autoplayTimeout:owl5
                .attr("data-autoplay-timeout")
                ? owl5.attr("data-autoplay-timeout")
                : 5000,
            rtl: false,
            autoHeight: true,
            autoWidth: true,
            margin:10,
            loop:
                owl5
                    .attr("data-carousel-loop")
                    .toLowerCase() === "true"
                    ? true
                    : false,
        });
    }


    $('.modal').modal({
        onCloseStart : function(elem){ $(elem).find('iframe').attr( 'src', function ( i, val ) { return val; }); }
    });
    AOS.init({ easing: 'ease-in-out-sine', duration: 1000, once : true });

    $('ul.mobile .menu-item-has-children a').click(function(){
        $(this).parent().find('ul.sub-menu').first().toggleClass('expanded');
    });
    $('.main_menu .toggle').click(function(){
        $(this).toggleClass('expanded');
        $('ul.mobile').toggleClass('expanded');
    })

    $.fancyConfirm = function(opts) {
        opts = $.extend(
            true,
            {
                title: "Are you sure?",
                message: "",
                okButton: "OK",
                noButton: "Cancel",
                callback: $.noop
            },
            opts || {}
        );

        $.fancybox.open({
            type: "html",
            src:
                '<div class="fc-content">' +
                "<h3>" +
                opts.title +
                "</h3>" +
                "<p>" +
                opts.message +
                "</p>" +
                '<div class="text-right confirmation">' +
                '<a data-value="0" data-fancybox-close>' +
                opts.noButton +
                "</a>" +
                '<button data-value="1" data-fancybox-close class="rounded">' +
                opts.okButton +
                "</button>" +
                "</div>" +
                "</div>",
            opts: {
                animationDuration: 350,
                animationEffect: "material",
                modal: true,
                baseTpl:
                    '<div class="fancybox-container fc-container" role="dialog" tabindex="-1">' +
                    '<div class="fancybox-bg"></div>' +
                    '<div class="fancybox-inner">' +
                    '<div class="fancybox-stage"></div>' +
                    "</div>" +
                    "</div>",
                afterClose: function(instance, current, e) {
                    var button = e ? e.target || e.currentTarget : null;
                    var value = button ? $(button).data("value") : 0;

                    opts.callback(value);
                }
            }
        });
    };

    $('.input-search-header').keypress(function (e) {
        if (e.which == 13) {
            $('form#searchform').submit();
            return false;
        }
    });

});

$(window).bind('load', function(){
    $('.testimonials-row').isotope({
        itemSelector: '.testimonial-card',
        percentPosition: true,
        masonry: {
            columnWidth: '.testimonial-card',
        }
    });
});



function carouselPrev(elem){
    // $(elem).hide();
    var elems = $(elem).parents('.carousel-container').find('.carousel')
    var moveRight = M.Carousel.getInstance(elems);
    moveRight.prev(1);
}

function carouselNext(elem){
    // $(elem).hide();
    var elems = $(elem).parents('.carousel-container').find('.carousel')
    var moveRight = M.Carousel.getInstance(elems);
    moveRight.next(1);
}

function GetDirections2(latitude, longitude){
    $('.map').find('iframe').attr('src',
        'https://www.google.com/maps/embed/v1/place?q='+latitude+'%2C'+longitude+'&key=AIzaSyDCKDGIY-bZdDovGd_zh0iScRJGhX7pTTA');
}

function GetDirections(latitude, longitude){
    var shift = 0.08;
    var lat = parseFloat(latitude);
    var lng = parseFloat(longitude);
    var bbox0a = lng - shift;
    var bbox0b = lat - shift;
    var bbox1a = lng + shift;
    var bbox1b = lat + shift;
    
    $('.map').find('iframe').attr('src',
        'https://www.openstreetmap.org/export/embed.html?bbox='+bbox0a+'%2C'+bbox0b+'%2C'+bbox1a+'%2C'+bbox1b+'&layer=mapnik&marker='+lat+'%2C'+lng);
}

function  DataSearch() {
    var value = $('.search_distributor').val().toLowerCase();

    $(".distributors-result .distributor-card").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });

    var distributors = $('.distributors-result .distributor-card:not([style*="display: none"])').length;

    if(distributors ==1 )
        $('.results-dist').html(distributors +" result found");
    else
        $('.results-dist').html(distributors +" results found")

}

function  NewsSearch() {
    var value = $('.search_news').val().toLowerCase();

    $(".news-row .news-card").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

function  CoursesSearch() {
    var value = $('.search_courses').val().toLowerCase();

    $(".courses-row .course-card").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

function changeQueryParameter(key , value ,location){
    var uri = window.location.href;
    var location_str = location ? "#"+location : "";
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf("?") !== -1 ? "&" : "?";
    if (uri.match(re)) {
        if( uri.indexOf('#') !== -1 ){
            uri = uri.replace(/#.*/, '');
        }
        window.location.href = uri.replace(re, "$1" + key + "=" + value + "$2") + location_str;
    } else {
        if( uri.indexOf('#') !== -1 ){
            uri = uri.replace(/#.*/, '');
        }
        window.location.href = uri + separator + key + "=" + value + location_str;
    }
}

function changeDescription(val){
    $('.label-tag').removeClass('active');
    $('#labelTag_'+val).addClass('active');

    $('.desc').removeClass('active');
    $('#desc_'+val).addClass('active');
}

function changeDetail(val){
    $('.sm-img').removeClass('active');
    $('#sm-img_'+val).addClass('active');

    $('.md-img').removeClass('active');
    $('#md-img_'+val).addClass('active');

    $('.detail-card').removeClass('active');
    $('#detail-card_'+val).addClass('active');
}

function FillApplication(courseName , category , training , level){

    $('.course-select').val(courseName);
    $('.category-select').val(category);
    $('.training-select').val(training);
    $('.level-select').val(level);

    $('html, body').animate({
        scrollTop: $(".application").offset().top
    },'slow');

}

function FillCourse(elem){
    $('.category-select').val(elem.find(':selected').data('category'));
    $('.training-select').val(elem.find(':selected').data('training'));
    $('.level-select').val(elem.find(':selected').data('level'));
}

function ShowMessage(Title, Text) {
    $.fancybox.open(
        '<div class="message"><h2>' + Title + "</h2><p>" + Text + "</p></div>"
    );
}

