<?php /* Template Name: Page | Beyond Products */ ?>

<?php get_header(); ?>
<?php
$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$meta =get_fields($post_id);

$home_label= "Home";


if(pll_current_language() == 'ar'){
    $home_label= "الصفحة الرئيسية";
}elseif (pll_current_language() == 'de'){
    $home_label = "Startseite";
}

?>

<div class="menu-spacer"></div>
<div class="beyond-products-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $meta['image']; ?>"></div>
            <div class="banner-info col-12 col-md-10">
                <div class="banner-label"><?php echo  $meta['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $meta['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

    <div class="page-description section-120-120">
        <div class="section-inner col-md-10 flex-column">
            <div class="block-label"><?php echo  $meta['block_label']; ?></div>
            <div class="page-desc op-45"><?php echo  nl2br($meta['block_description']); ?></div>
        </div>
    </div>

    <div class="grid-display section-80-80 gray">
        <div class="section-inner col-md-10 flex-column">
            <div class="grid-inner">
                <?php $i=0; foreach ($meta['grid_information']AS $value){ $i++; ?>
                    <div class="grid-card <?php if($i%2){ echo 'even' ;}else{ echo 'odd'; }?>">
                        <div class="grid-virtual" <?php if($i%2){?> data-aos="fade-up" data-aos-delay="<?php echo 200+$i*50; ?>"  <?php }else{?> data-aos="fade-up" data-aos-delay="<?php echo 150+$i*50; ?>" <?php } ?>>
                            <div class="line <?php if($i%2){}else{ echo 'margin-left-40'; }?>"></div>
                            <div class="image ratio-5-3 cover" style="background-image: url('<?php echo  $value['image'] ?>')"></div>
                        </div>
                        <div class="grid-info <?php if($i%2){ echo 'padding-left-140';}else{ echo 'padding-right-140'; }?>" <?php if($i%2){?> data-aos="fade-up" data-aos-delay="<?php echo 200+$i*50; ?>"  <?php }else{?> data-aos="fade-up" data-aos-delay="<?php echo 150+$i*50; ?>" <?php } ?>>
                            <div class="sm-label"><?php echo $value['label'] ?></div>
                            <div class="desc op-45"><?php echo nl2br($value['description']) ?></div>

                            <?php if($value['page_link']){
                                $Page_ID =url_to_postid($value['page_link'] );
                                $PostInfo = get_post($Page_ID);
                                ?>
                                <a href="<?php echo $value['page_link'] ?>" class="c-button">
                                    <div class="btn-text"><?php echo $PostInfo->label?></div>
                                </a>
                            <?php } ?>

                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>

</div>


<?php get_footer(); ?>
