<?php /* Template Name: Page | Testimonials */ ?>

<?php get_header(); ?>
<?php

//$meta = get_fields('566');

$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$meta  =get_fields($post_id);

$home_label= "Home";
$lang_label= "en";
$lang_ext = "" ;


if(pll_current_language() == 'ar'){
    $home_label= "الصفحة الرئيسية";
    $lang_label = pll_current_language();
    $lang_ext = "_ar";
}elseif (pll_current_language() == 'de'){
    $home_label = "Startseite";
    $lang_label = pll_current_language();
    $lang_ext = "_de";
}


?>

<div class="menu-spacer"></div>
<div class="testimonials-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image cover" style="background-image: url('<?php echo  $meta['image']; ?>')"></div>
            <div class="banner-info col-12 col-md-10">
                <div class="banner-label"><?php echo  $meta['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $meta['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

    <div class="testimonials section-100-100 bg-03">
        <div class="section-inner col-md-10 flex-column">
            <div class="testimonials-row">
                <?php
                    $testimonials =get_posts( array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'category_name' => 'testimonial'.$lang_ext,
                        'posts_per_page'=>-1,
                        'lang'=>$lang_label

                    ));

                if($testimonials){
                    $i=0;
                    foreach ($testimonials as $testimonial){
                        $i++;
                ?>
                        <div class="testimonial-card float-left" data-aos="fade-up" data-aos-delay="<?php echo 100+$i*10; ?>" >
                            <div class="quote" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/quotes.png')"></div>
                            <div class="testimnonial-info">
                                <div class="testimonial"><?php echo nl2br($testimonial->testimonial) ?></div>
                                <div class="fullName"><?php echo $testimonial->full_name ?></div>
                                <div class="position"><?php echo $testimonial->position ?></div>
                                <?php $image_src = wp_get_attachment_image_src($testimonial->testimonial_image,'large'); ?>

                                <a data-fancybox="images" href="<?php echo $image_src[0]  ?>"  class="item-card-container">
                                    <div class="image"><?php echo wp_get_attachment_image( $testimonial->testimonial_image);?></div>
                                </a>
                            </div>
                        </div>
                <?php
                    }
                }
                ?>
            </div>

        </div>
    </div>
</div>


<?php get_footer(); ?>
