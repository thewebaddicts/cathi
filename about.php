<?php /* Template Name: Page | About us */ ?>

<?php get_header(); ?>
<?php

if(pll_current_language() == "ar"){
    $home_label= "الصفحة الرئيسية";
    $partners = get_fields('1137');
}elseif (pll_current_language() == "de"){
    $partners = get_fields('1136');
    $home_label = "Startseite";
}else{
    $home_label= "Home";
    $partners= get_fields('5');
}


$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$meta =get_fields($post_id);
?>

<div class="menu-spacer"></div>
<div class="about-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $meta['image']; ?>"></div>
            <div class="banner-info col-12 col-md-10">
                <div class="banner-label"><?php echo  $meta['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $meta['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

    <a name="our-philosophy" class="a-name" ></a>
    <?php  if(count($meta['block_information'])>0){?>
        <div class=" display-block">
            <div class="display-block-inner col-md-10">
                <div class="block-label" data-aos="fade-up" data-aos-delay="100"><?php echo  $meta['block_information'][0]['label']; ?></div>
                <div class="block-info">
                    <div class="block-description " data-aos="fade-up" data-aos-delay="100"><?php echo  nl2br($meta['block_information'][0]['description']); ?></div>
                    <div class="block-visual" data-aos="fade-up" data-aos-delay="100">
                        <div class="image ratio-5-3 cover" style="background-image:url('<?php echo  $meta['block_information'][0]['image'] ?>') "></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <a name="our-values" class="a-name" ></a>
    <div class="grid-display section-120-120 gray">
        <div class="section-inner col-md-10 flex-column">
            <div class="section-label" data-aos="fade-up"><?php if(isset($meta['values_section'])) echo $meta['values_section'] ?></div>
            <div class="grid-inner">
                <?php $i=0; foreach ($meta['grid_information']AS $value){ $i++; ?>
                    <div class="grid-card <?php if($i%2){ echo 'even' ;}else{ echo 'odd'; }?>">
                        <div class="grid-virtual" <?php if($i%2){?> data-aos="fade-up" data-aos-delay="<?php echo 200+$i*50; ?>"  <?php }else{?> data-aos="fade-up" data-aos-delay="<?php echo 150+$i*50; ?>" <?php } ?>>
                            <div class="line <?php if($i%2){}else{ echo 'margin-left-40'; }?>"></div>
                            <div class="image ratio-5-3 cover" style="background-image: url('<?php echo  $value['image'] ?>')"></div>
                        </div>
                        <div class="grid-info <?php if($i%2){ echo 'padding-left-140';}else{ echo 'padding-right-140'; }?>" <?php if($i%2){?> data-aos="fade-up" data-aos-delay="<?php echo 200+$i*50; ?>"  <?php }else{?> data-aos="fade-up" data-aos-delay="<?php echo 150+$i*50; ?>" <?php } ?>>
                            <div class="sm-label"><?php echo $value['label'] ?></div>
                            <div class="desc op-45"><?php echo nl2br($value['description']) ?></div>

                            <?php if($value['page_link']){
                                $Page_ID =url_to_postid($value['page_link'] );
                                $PostInfo = get_post($Page_ID);
                                ?>
                                <a href="<?php echo $value['page_link'] ?>" class="c-button">
                                    <div class="btn-text"><?php echo $PostInfo->label?></div>
                                </a>
                            <?php } ?>

                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>

    <a name="our-sustainability" class="a-name" ></a>
    <?php  if(count($meta['block_information'])>1){?>
        <div class=" display-block">
            <div class="display-block-inner col-md-10">
                <div class="block-label" data-aos="fade-up" data-aos-delay="100"><?php echo  $meta['block_information'][1]['label']; ?></div>
                <div class="block-info" data-aos="fade-up" data-aos-delay="100">
                    <div class="block-description" data-aos="fade-up" data-aos-delay="100"><?php echo  nl2br($meta['block_information'][1]['description']); ?></div>
                    <div class="block-visual">
                        <div class="image ratio-5-3 cover" style="background-image:url('<?php echo  $meta['block_information'][1]['image'] ?>') "></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <a name="who-we-are" class="a-name" ></a>
    <div class="team section-100-100 bg-gray">
        <div class="section-inner col-md-10 flex-column">
            <div class="section-label" data-aos="fade-up"><?php if(isset($meta['team_section'])) echo $meta['team_section'] ?></div>
            <div class="team-row">
                <?php $i=0; foreach ($meta['team_information'] AS $member){ $i++; ?>
                    <div class="team-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                       <div class="image ratio-1-1 cover" style="background-image: url('<?php echo $member['image']?>')"></div>

                        <div class="sm-label"><?php echo $member['full_name']?></div>
                        <div class="description op-45"><?php echo nl2br($member['description'])?></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <a name="partners" class="a-name" ></a>
    <div class="partners section-100-100">
        <div class="section-inner col-md-10 flex-column">
            <div class="section-label" data-aos="fade-up"><?php if(isset($partners['partners_section'])) echo $partners['partners_section'] ?></div>
            <div class="partners-row">
                <?php $i=0; foreach ($partners['partners_logo'] AS $partner){ $i++; ?>
                    <div class="partner-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                        <div class="logo contain" style="background-image: url('<?php echo $partner['logo'] ?>')"></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
