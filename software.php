<?php /* Template Name: Page | Software */ ?>

<?php get_header(); ?>
<?php
//$meta = get_fields('60');

$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$meta  =get_fields($post_id);
$lang_label= "en";
$lang_ext = "" ;

$home_label= "Home";
if(pll_current_language() == 'ar'){
    $home_label= "الصفحة الرئيسية";
    $lang_label = pll_current_language();
    $lang_ext = "_ar";
}elseif (pll_current_language() == 'de'){
    $home_label = "Startseite";
    $lang_label = pll_current_language();
    $lang_ext = "_de";
}

?>

<div class="menu-spacer"></div>
<div class="software-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image cover parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $meta['image']; ?>"></div>
            <div class="banner-info col-10">
                <div class="banner-label"><?php echo  $meta['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $meta['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

	<a id="software-functionalities" class="a-name"></a>
    <?php  if(count($meta['block_information'])>0){?>
        <div class=" display-block">
            <div class="display-block-inner col-md-10">
                <div class="block-label" data-aos="fade-up" data-aos-delay="100"><?php echo  $meta['block_information'][0]['label']; ?></div>
                <div class="block-info">
                    <div class="block-description " data-aos="fade-up" ><?php echo  nl2br($meta['block_information'][0]['description']); ?></div>
                    <div class="block-visual" data-aos="fade-up" data-aos-delay="100">
                        <div class="image ratio-5-3 cover" style="background-image:url('<?php echo  $meta['block_information'][0]['image'] ?>') "></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php $i=0; foreach ($meta['functionalities']AS $functionality){ $i++; ?>
    <div class="functionalities section-100-100 " data-aos="fade-up">
        <div class="section-inner col-md-10 flex-column">
            <div class="section-label" data-aos="fade-up" data-aos-delay="100" data-aos-offset="-200"><?php echo  $functionality['label']; ?></div>
            <div class="information-row">
                <?php $i=0; foreach ($functionality['information'] AS $info){ $i++; ?>
                    <div class="info-card border-left-3" data-aos="fade-up" data-aos-delay="<?php echo 150+$i*50; ?>" data-aos-offset="-200"><?php echo $info['title'] ?></div>
                <?php }?>
            </div>
        </div>
    </div>
    <?php } ?>

    <a name="section1" class="a-name"></a>

    <div class="modules section-100-100 ">
        <div class="section-inner col-md-10 flex-column">
            <a id="software-interventions" class="a-name"></a>
            <div class="interventions" id="interventions">
                <div class="sm-label" data-aos="fade-up" data-aos-delay="100"><?php if(isset($meta['interventions'])) echo $meta['interventions'] ?></div>
                <div class="products-row">
                    <?php
                    $modules =get_posts( array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'category_name' => 'software_modules'.$lang_ext,
                        'posts_per_page'=>-1,
                        'lang'=>$lang_label
                    ));

                    if( $modules){
                        foreach ( $modules as $module){
                            ?>
                            <a href="<?php echo get_permalink( $module->ID ) ?>" class="product-card" data-aos="fade-up" data-aos-delay="<?php echo 30+$i*5; ?>">
                                <div class="product-image">
                                    <?php $image_src = wp_get_attachment_image_src($module->thumbnail,'large'); ?>
                                    <div class="image cover ratio-4-3 image-loading" style="background-image: url('<?php echo $image_src[0] ?>')"></div>

                                </div>
                                <div class="product-info">
                                    <div class="product-label"><?php echo $module->label ?></div>
                                    <div class="product-sublabel"><?php echo $module->description ?></div>
                                </div>

                            </a>
                        <?php }} ?>
                </div>
            </div>
            
            <a id="software-beyond"></a>
            <div class="beyond-interventions">
                <div class="sm-label" data-aos="fade-up" data-aos-delay="100"><?php if(isset($meta['beyond_interventions'])) echo $meta['beyond_interventions'] ?></div>
                <div class="products-row">
                    <?php

                    if(isset($_GET['module'])){
                        $filterModule = $_GET['module'];
                    }else{
                        $filterModule = null ;
                    }
                    $products = get_pages( array(
                        'child_of'    => $post_id,
                        'meta_key' => 'intervention',
                        'meta_value' => 'Beyond Interventions',
                    ));

                    $i = 0;

                    if($products){
                        foreach ($products as $product){
                            $i++;
                            ?>
                            <a href="<?php echo get_permalink( $product->ID ) ?>" class="product-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                                <div class="product-image">
                                    <?php $image_src = wp_get_attachment_image_src($product->thumbnail_image,'large'); ?>
                                    <div class="image cover ratio-4-3" style="background-image: url('<?php echo $image_src[0] ?>')"></div>

                                </div>
                                <div class="product-info">
                                    <div class="product-label"><?php echo $product->label ?></div>
                                    <div class="product-sublabel"><?php echo $product->sublabel ?></div>
                                </div>

                            </a>

                        <?php }}?>

                </div>
            </div>
        </div>
    </div>

</div>


<?php get_footer(); ?>


