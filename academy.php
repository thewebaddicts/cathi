<?php /* Template Name: Page | CATHI Academy */ ?>

<?php get_header(); ?>
<?php

//$meta = get_fields('64');

$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$meta =get_fields($post_id);

$lang_label= "en";
$lang_ext = "" ;
$home_label= "Home";
$search_label= "Search...";

if(pll_current_language() == 'ar'){
    $lang_label = pll_current_language();
    $lang_ext = "_ar";
    $home_label= "الصفحة الرئيسية";
    $search_label= "ابحث";
}elseif (pll_current_language() == 'de'){
    $lang_label = pll_current_language();
    $lang_ext = "_de";
    $home_label = "Startseite";
    $search_label = "Suche...";
}

?>

<div class="menu-spacer"></div>
<div class="academy-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $meta['image']; ?>"></div>
            <div class="banner-info col-12 col-md-10">
                <div class="banner-label"><?php echo  $meta['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $meta['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

    <div class="block">
            <div class="display-block">
                <?php $i=0; foreach ($meta['block_information']AS $block){ $i++; ?>
                <div class="display-block-inner col-md-10">
                    <div class="block-label" data-aos="fade-up" data-aos-delay="100"><?php echo  $block['label']; ?></div>
                    <div class="block-info">
                        <div class="block-description" data-aos="fade-up" data-aos-delay="100"><?php echo  nl2br($block['description']); ?></div>
                        <div class="block-visual" data-aos="fade-up" data-aos-delay="100">
                            <div class="image ratio-5-3 cover" style="background-image:url('<?php echo  $block['image'] ?>') "></div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
    </div>

    <?php if($meta['show_courses']==1){?>

    <a name="section2" class="a-name"></a>
    <div class="Filters" data-aos="fade-up">
        <div class="section-inner col-md-10">
            <div class="category-filter filter-inner">
                <select class="browser-default" onchange="changeQueryParameter('category',$(this).val(),'section2')">
                    <option value="" <?php if (!isset($_GET["category"])){?> selected  <?php  } ?> disabled><?php if(isset($meta['category_label'])) echo $meta['category_label'] ?></option>
                    <?php
                        $categories =get_posts( array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'category_name' => 'course_category'.$lang_ext,
                            'posts_per_page'=>-1,
                            'lang'=>$lang_label
                        ));

                    if($categories){
                        foreach ($categories as $category){
                            ?>
                            <option value="<?php echo $category->label ?>" <?php if (isset($_GET["category"]) && $_GET["category"] == $category->label){?> selected  <?php  } ?>><?php echo $category->label ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="training-filter filter-inner">
                <select class="browser-default" onchange="changeQueryParameter('training',$(this).val(),'section2')" >
                    <option value="" <?php if (!isset($_GET["training"])){?> selected  <?php  } ?> disabled><?php if(isset($meta['training_selection_label'])) echo $meta['training_selection_label'] ?></option>
                    <?php
                    $training =get_posts( array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'category_name' => 'course_training_selection'.$lang_ext,
                        'posts_per_page'=>-1,
                        'lang'=>$lang_label
                    ));

                    if($training){
                        foreach ($training as $selection){
                            ?>
                            <option value="<?php echo $selection->label ?>" <?php if (isset($_GET["training"]) && $_GET["training"] == $selection->label){?> selected  <?php  } ?>><?php echo $selection->label ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="level-filter filter-inner">
                <select class="browser-default" onchange="changeQueryParameter('level',$(this).val(),'section2')">
                    <option value="" <?php if (!isset($_GET["level"])){?> selected  <?php  } ?> disabled><?php if(isset($meta['level_label'])) echo $meta['level_label'] ?></option>
                    <?php
                    $levels =get_posts( array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'category_name' => 'course_level'.$lang_ext,
                        'posts_per_page'=>-1,
                        'lang'=>$lang_label
                    ));

                    if($levels ){
                        foreach ($levels  as $level){
                            ?>
                            <option value="<?php echo $level->label ?>" <?php if (isset($_GET["level"]) && $_GET["level"] == $level->label){?> selected  <?php  } ?>><?php echo $level->label ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <div role="search"  id="searchBlock" class="searchBlock padding-left-50 mobile-padding-0">
                <div type="submit" id="search_courses_btn" onclick="CoursesSearch()">
                    <div class="contain search-icon margin-right-10" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/search.png')"></div>
                </div>
                <input type="text"  name="search_input" id="search_input" placeholder="<?php echo  $search_label ?>" class="browser-default search_input search_courses" onkeyup="CoursesSearch()">
            </div>

        </div>
    </div>

    <div class="courses section-50-50">
        <div class="section-inner col-md-10">
            <div class="courses-row">
                <?php

                $i=0; foreach ($meta['courses']AS $course){;

                    $check = 0;
                    $query="";

                    if (isset($_GET["category"]))
                    {
                        $check = 1;
                        if($query != ""){
                            $query .= " && ";
                        }
                        $query .= "'".$course["category"]->label ."' == '". $_GET["category"]."'";
                    }
                    if (isset($_GET["training"]))
                    {
                        $check = 1;
                        if($query != ""){
                            $query .= " && ";
                        }
                        $query .= "'".$course["training_selection"]->label ."' == '". $_GET["training"] ."'";
                    }
                    if (isset($_GET["level"]))
                    {
                        $check = 1;
                        if($query != ""){
                            $query .= " && ";
                        }
                        $query .= "'".$course["level"]->label  ."' == '". $_GET["level"] ."'";
                    }

                    if(($check == 1 && eval("return $query;") ) || $check == 0){
                        $i++
                    ?>
                    <div class="course-card" <?php if($i%2){?> data-aos="fade-up" data-aos-delay="<?php echo 200+$i*50; ?>"  <?php }else{?> data-aos="fade-up" data-aos-delay="<?php echo 150+$i*50; ?>" <?php } ?> >
                        <div class="course-header">
                            <div class="label"><?php echo $course['course_label'] ?></div>
                            <div class="tags-row">
                                <?php
                                $featured_category = $course['category'];
                                if( $featured_category ){
                                    ?>
                                    <div class="category-tag margin-right-10"><?php  echo $featured_category->label ?></div>
                                <?php  }?>

                                <?php
                                $featured_training = $course['training_selection'];
                                if( $featured_training ){
                                        ?>
                                        <div class="training-tag margin-right-10"><?php  echo $featured_training->label ?></div>
                                    <?php  }?>

                                <?php
                                $featured_level = $course['level'];
                                if( $featured_level){
                                        ?>
                                        <div class="level-tag margin-right-10"><?php  echo $featured_level->label ?></div>
                                    <?php  }?>
                            </div>
                        </div>
                        <div class="course-body">
                            <div class="information">
                                <div class="description info-block">
                                    <div class="info-label"><?php if(isset($meta['course_description_label'])) echo $meta['course_description_label'] ?></div>
                                    <div class="info-desc">
                                        <?php echo nl2br($course['course_description']) ?>
                                    </div>
                                </div>
                                <div class="modules info-block">
                                    <div class="info-label"><?php if(isset($meta['course_modules_label'])) echo $meta['course_modules_label'] ?></div>
                                    <div class="info-desc">
                                        <?php echo nl2br($course['course_modules']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="c-button" onclick="FillApplication('<?php echo $course['course_label'] ?>','<?php echo $course['category']->label ?>','<?php echo $course['training_selection']->label ?>','<?php echo $course['level']->label ?>')">
                                <span class="btn-text"><?php if(isset($meta['course_button_label'])) echo $meta['course_button_label'] ?></span>
                            </div>

                        </div>
                    </div>
                <?php }} ?>

            </div>
        </div>
    </div>

    <div class="application section-100-100 bg-gray">
        <div class="section-inner application-inner col-md-10 col-lg-6 flex-column">
            <div class="form-label" data-aos="fade-up" data-aos-delay="100"><?php if(isset($meta['form_label']))  echo $meta['form_label'] ?></div>
            <div class="contact-form" data-aos="fade-up" data-aos-delay="200">
                <form  action="" method="POST" enctype="multipart/form-data" class="ContactForm">
                    <div class="course-info">
                        <div class="input-label"><?php if(isset($meta['application_course_label']))  echo $meta['application_course_label'] ?></div>
                        <div class="row-100">
                            <select class="browser-default cathi-select course-select padding-20" name="course-select" onchange="FillCourse($(this))" required>
                                <option value="" selected disabled><?php if(isset($meta['course_name_label']))  echo $meta['course_name_label'] ?></option>
                                <?php foreach ($meta['courses']AS $course){?>
                                        <option value="<?php echo $course['course_label'] ?>" data-category="<?php echo $course['category']->label ?>" data-training="<?php echo $course['training_selection']->label ?>" data-level="<?php echo $course['level']->label ?>"><?php echo $course['course_label'] ?></option>
                               <?php } ?>
                            </select>

                            <select class="browser-default cathi-select category-select padding-20" name="category-select" required>
                                <option value="" selected disabled><?php if(isset($meta['category_label']))  echo $meta['category_label']?></option>
                                <?php
                                $categories =get_posts( array(
                                    'post_type' => 'post',
                                    'post_status' => 'publish',
                                    'category_name' => 'course_category'.$lang_ext,
                                    'posts_per_page'=>-1,
                                    'lang'=>$lang_label
                                ));

                                if($categories){
                                    foreach ($categories as $category){
                                        ?>
                                        <option value="<?php echo $category->label ?>"><?php echo $category->label ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                            <select class="browser-default cathi-select training-select padding-20" name="training-select" required>
                                <option value="" selected disabled><?php if(isset($meta['training_selection_label']))  echo $meta['training_selection_label']?></option>
                                <?php
                                $training =get_posts( array(
                                    'post_type' => 'post',
                                    'post_status' => 'publish',
                                    'category_name' => 'course_training_selection'.$lang_ext,
                                    'posts_per_page'=>-1,
                                    'lang'=>$lang_label
                                ));

                                if($training){
                                    foreach ($training as $selection){
                                        ?>
                                        <option value="<?php echo $selection->label ?>"><?php echo $selection->label ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                            <select class="browser-default cathi-select level-select padding-20" name="level-select" required>
                                <option value="" selected disabled><?php if(isset($meta['level_label']))  echo $meta['level_label']?></option>
                                <?php
                                $levels =get_posts( array(
                                    'post_type' => 'post',
                                    'post_status' => 'publish',
                                    'category_name' => 'course_level'.$lang_ext,
                                    'posts_per_page'=>-1,
                                    'lang'=>$lang_label
                                ));

                                if($levels ){
                                    foreach ($levels  as $level){
                                        ?>
                                        <option value="<?php echo $level->label ?>"><?php echo $level->label ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="border-line pos-left-40"></div>

                    </div>
                    <div class="user-info">
                        <div class="row-50">
                            <div class="input-label"><?php if(isset($meta['first_name_label']))  echo $meta['first_name_label']?></div>
                            <input type="text" name="first_name" placeholder="<?php if(isset($meta['first_name_label']))  echo $meta['first_name_label']?>" class="browser-default padding-20" required>
                        </div>
                        <div class="row-50">
                            <div class="input-label"><?php if(isset($meta['last_name_label']))  echo $meta['last_name_label']?></div>
                            <input type="text" name="last_name" placeholder="<?php if(isset($meta['last_name_label']))  echo $meta['last_name_label']?>" class="browser-default padding-20" required>
                        </div>
                        <div class="row-100">
                            <div class="input-label"><?php if(isset($meta['email_label']))  echo $meta['email_label']?></div>
                            <input type="email" name="email" placeholder="<?php if(isset($meta['email_label']))  echo $meta['email_label']?>" class="browser-default padding-20" required>
                        </div>
                        <div class="row-50">
                            <div class="input-label"><?php if(isset($meta['phone_number']))  echo $meta['phone_number']?></div>
                            <input type="text" name="phone_number" placeholder="<?php if(isset($meta['phone_number']))  echo $meta['phone_number']?>" class="browser-default padding-20" required>
                        </div>
                        <div class="row-50">
                            <div class="input-label"><?php if(isset($meta['country_label']))  echo $meta['country_label']?></div>
                            <select class="browser-default country-select"  name="country-select" required>
                                <option value="" selected disabled><?php if(isset($meta['country_label']))  echo $meta['country_label']?></option>
                                <?php
                                global $wpdb;
                                $result = $wpdb->get_results ( "SELECT * FROM countries" );
                                foreach ( $result as $country ) {
                                    ?>
                                    <option value="<?php echo $country->name ?>"><?php echo $country->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="row-100">
                            <div class="input-label"><?php if(isset($meta['message_label']))  echo $meta['message_label']?></div>
                            <textarea  name="message" placeholder="<?php if(isset($meta['message_label']))  echo $meta['message_label']?>" class="browser-default padding-20" required></textarea>
                        </div>

                        <div class="g-recaptcha" data-sitekey="6LeGXKwaAAAAAGF4O6ONFFhiilk5NDtHNsZvrMo5"></div>

                        <div class="app-submit">
                            <button type="submit" name="submit" class="c-button" data-aos="fade-up" data-aos-delay="300">
                                <div class="btn-text"><?php if(isset($meta['form_button_label']))  echo $meta['form_button_label']?></div>
                            </button>
                        </div>

                    </div>



                </form>
            </div>
        </div>
    </div>

    <?php }else{ ?>

        <div class="application section-80-80 bg-gray">
            <div class="section-inner application-inner col-md-10 col-lg-6 flex-column">
                <div class="form-label" data-aos="fade-up" data-aos-delay="100"><?php if(isset($meta['form_label']))  echo $meta['form_label'] ?></div>
                <div class="contact-form" data-aos="fade-up" data-aos-delay="200">
                    <form  action="" method="POST" enctype="multipart/form-data" class="ContactForm">
                        <div class="user-info">
                            <div class="row-50">
                                <div class="input-label"><?php if(isset($meta['first_name_label']))  echo $meta['first_name_label']?></div>
                                <input type="text" name="first_name" placeholder="<?php if(isset($meta['first_name_label']))  echo $meta['first_name_label']?>" class="browser-default padding-20" required>
                            </div>
                            <div class="row-50">
                                <div class="input-label"><?php if(isset($meta['last_name_label']))  echo $meta['last_name_label']?></div>
                                <input type="text" name="last_name" placeholder="<?php if(isset($meta['last_name_label']))  echo $meta['last_name_label']?>" class="browser-default padding-20" required>
                            </div>
                            <div class="row-100">
                                <div class="input-label"><?php if(isset($meta['email_label']))  echo $meta['email_label']?></div>
                                <input type="email" name="email" placeholder="<?php if(isset($meta['email_label']))  echo $meta['email_label']?>" class="browser-default padding-20" required>
                            </div>
                            <div class="row-50">
                                <div class="input-label"><?php if(isset($meta['phone_number']))  echo $meta['phone_number']?></div>
                                <input type="text" name="phone_number" placeholder="<?php if(isset($meta['phone_number']))  echo $meta['phone_number']?>" class="browser-default padding-20" required>
                            </div>
                            <div class="row-50">
                                <div class="input-label"><?php if(isset($meta['country_label']))  echo $meta['country_label']?></div>
                                <select class="browser-default country-select"  name="country-select" required>
                                    <option value="" selected disabled><?php if(isset($meta['country_label']))  echo $meta['country_label']?></option>
                                    <?php
                                    global $wpdb;
                                    $result = $wpdb->get_results ( "SELECT * FROM countries" );
                                    foreach ( $result as $country ) {
                                        ?>
                                        <option value="<?php echo $country->name ?>"><?php echo $country->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="row-100">
                                <div class="input-label"><?php if(isset($meta['message_label']))  echo $meta['message_label']?></div>
                                <textarea  name="message" placeholder="<?php if(isset($meta['message_label']))  echo $meta['message_label']?>" class="browser-default padding-20" required></textarea>
                            </div>

                            <div class="g-recaptcha" data-sitekey="6LeGXKwaAAAAAGF4O6ONFFhiilk5NDtHNsZvrMo5"></div>

                            <div class="app-submit">
                                <button type="submit" name="submitContact" class="c-button" data-aos="fade-up" data-aos-delay="300">
                                    <div class="btn-text"><?php if(isset($meta['form_button_label']))  echo $meta['form_button_label']?></div>
                                </button>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>

    <?php } ?>

</div>


<?php get_footer(); ?>

<script type ='text/JavaScript'>
    <?php

    if(isset($_POST['submit'])){
        // Get the submitted form data
        $postData = $_POST;
        $course = $_POST['course-select'];
        $category = $_POST['category-select'];
        $training= $_POST['training-select'];
        $level= $_POST['level-select'];
        $name = $_POST['first_name'].' '.$_POST['last_name'];
        $email = $_POST['email'];
        $phone_number = $_POST['phone_number'];
        $country= $_POST['country-select'];
        $subject = 'New Course Application';
        $message = $_POST['message'];

        if(!empty($_POST['g-recaptcha-response']))
        {
            $secret = '6LeGXKwaAAAAAMQzuSoPqh-i5OEtVGMZlz6eche9';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success){
                if(!empty($course) && !empty($category) && !empty($training) && !empty($level) &&!empty($email) && !empty($name) && !empty($subject) && !empty($message) && !empty($phone_number) && !empty($country)){

                    // Validate email
                    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
                        echo 'Please enter your valid email.';
                    }else{

                        // Recipient
                        $toEmail = trim($meta['send_form_to_email']);

                        // Subject
                        $emailSubject = 'New Course Application';

                        // Message
                        $htmlContent = '<h2>New Course Application</h2>
                                    <p><b style="color:black">Course Name:</b> '.$course.'</p>
                                    <p><b style="color:black">Category:</b> '.$category.'</p>
                                    <p><b style="color:black">Training Selection:</b> '.$training.'</p>
                                    <p><b style="color:black">Level:</b> '.$level.'</p>
                                    <br>
                                    <p><b style="color:black">Full Name:</b> '.$name.'</p>
                                    <p><b style="color:black">Email:</b> '.$email.'</p>
                                    <p><b style="color:black">Country:</b> '.$country.'</p>
                                    <p><b style="color:black">Phone Number:</b> '.$phone_number.'</p>
                                    <p><b style="color:black">Message:</b><br/>'.$message.'</p>';

                        $response = sendmail($emailSubject, $toEmail, $htmlContent);

                        if($response == 1){
                            echo "ShowMessage('Thank You' ,'Your Application has been submitted')";
                        }else if($response == 0 ){
                            echo "ShowMessage('Sorry' ,'Could not submit you application, try again later!')";
                        }
                    }
                }else{
                    echo "ShowMessage('Error' ,'please fill all fields')";
                }

            } else{
                echo "ShowMessage('Recaptcha' ,'Error in submitting Recaptcha')";
            }

        }else{
            echo "ShowMessage('Recaptcha' ,'Recaptcha field required')";
        }

    }

    if(isset($_POST['submitContact'])){
        // Get the submitted form data
        $postData = $_POST;
        $name = $_POST['first_name'].' '.$_POST['last_name'];
        $email = $_POST['email'];
        $phone_number = $_POST['phone_number'];
        $country= $_POST['country-select'];
        $subject = 'New Contact Application';
        $message = $_POST['message'];

        if(!empty($_POST['g-recaptcha-response']))
        {
            $secret = '6LeGXKwaAAAAAMQzuSoPqh-i5OEtVGMZlz6eche9';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success){
                if(!empty($email) && !empty($name) && !empty($subject) && !empty($message) && !empty($phone_number) && !empty($country)){

                    // Validate email
                    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
                        echo 'Please enter your valid email.';
                    }else{

                        // Recipient
                        $toEmail = trim($meta['send_form_to_email']);

                        // Subject
                        $emailSubject = 'New Contact Application';

                        // Message
                        $htmlContent = '<h2>New Course Application</h2>
                                    <p><b style="color:black">Full Name:</b> '.$name.'</p>
                                    <p><b style="color:black">Email:</b> '.$email.'</p>
                                    <p><b style="color:black">Country:</b> '.$country.'</p>
                                    <p><b style="color:black">Phone Number:</b> '.$phone_number.'</p>
                                    <p><b style="color:black">Message:</b><br/>'.$message.'</p>';

                        $response = sendmail($emailSubject, $toEmail, $htmlContent);

                        if($response == 1){
                            echo "ShowMessage('Thank You' ,'Your Application has been submitted')";
                        }else if($response == 0 ){
                            echo "ShowMessage('Sorry' ,'Could not submit you application, try again later!')";
                        }

                    }
                }else{
                    echo "ShowMessage('Error' ,'please fill all fields')";
                }
            } else{
                echo "ShowMessage('Recaptcha' ,'Error in submitting Recaptcha')";
            }

        }else{
            echo "ShowMessage('Recaptcha' ,'Recaptcha field required')";
        }

    }
    ?>
</script>
