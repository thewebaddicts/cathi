<?php /* Template Name: Template | Search Page */ ?>
<?php get_header(); ?>

<?php

global $query_string;

wp_parse_str( $query_string, $search_query );
if(is_array($search_query) && array_key_exists("search",$search_query)){
    $search = $search_query['search'];
}else{
    $search = "";
}

$search_label= "Search...";


if(pll_current_language() == 'ar'){
    $search_label= "ابحث";
}elseif (pll_current_language() == 'de'){
    $search_label = "Suche...";
}
?>

<div class="menu-spacer"></div>
<div class="search-page page-top">
    <div class="section-search">
        <div class="section-inner col-md-6 flex-column">
            <form role="search" method="get" id="searchformPage" class="searchformPage">
                <input type="text" value="<?php echo $search; ?>" name="search" id="searchInputPage" placeholder="<?php echo $search_label ?>" class="browser-default searchInputPage">
                <button type="submit" id="searchsubmitPage">
                    <div class="contain search-icon" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/search.png')"></div>
                </button>
            </form>
        </div>
    </div>

    <?php if($search!='') {
        $search1 = new WP_Query(array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'templates/hardware.php',
            's' => $search
        ));

        $search2 = new WP_Query(array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'templates/software.php',
            's' => $search
        ));

        $search3 = new WP_Query(array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'templates/news.php',
            's' => $search
        ));

    ?>

    <div class=" search-results section-50-50">
        <div class="section-inner col-md-10 ">
            <div class="results-container">
                <?php $i=0; foreach ($search1->posts AS $post){ $i++;   ?>
                    <a href="<?php echo $post->guid; ?>" data-aos="fade-up" data-aos-delay="<?php echo $i*100; ?>" class="search-result-card">
                        <span><?php echo get_the_title( $post->post_parent ); ?></span>
                        <h6 class="t-lines-2"><?php echo $post->post_title; ?></h6>
                    </a>
                <?php } ?>

                <?php $i=0; foreach ($search2->posts AS $post){ $i++;   ?>
                    <a href="<?php echo $post->guid; ?>" data-aos="fade-up" data-aos-delay="<?php echo $i*100; ?>" class="search-result-card">
                        <span><?php echo get_the_title( $post->post_parent ); ?></span>
                        <h6 class="t-lines-2" ><?php echo $post->post_title; ?></h6>
                    </a>
                <?php } ?>

                <?php $i=0; foreach ($search3->posts AS $post){ $i++;   ?>
                    <a href="<?php echo $post->guid; ?>" data-aos="fade-up" data-aos-delay="<?php echo $i*100; ?>" class="search-result-card">
                        <span><?php echo get_the_title( $post->post_parent ); ?></span>
                        <h6 class="t-lines-2"><?php echo $post->post_title; ?></h6>
                    </a>
                <?php } ?>
            </div>


        </div>
    </div>

    <?php }?>
</div>
<?php get_footer(); ?>


