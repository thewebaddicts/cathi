<?php

function cathi_theme_support(){
    //adds dynamic tags support
    add_theme_support('title-tag');
    add_theme_support('custom-logo');
}
add_action('after_setup_theme','cathi_theme_support');


function cathi_menu(){

    $locations = [
        'primary' => "Desktop Primary Menu",
        'footer' => "Desktop Footer Menu",
        'mobile' => "Mobile Menu",
        'language' => "Language Switcher"
    ];
    register_nav_menus($locations);
}

add_action('init','cathi_menu');

function cathi_register_styles(){
    wp_enqueue_style('cathi_font-awesome','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css', [], '5.15.1','all');
    wp_enqueue_style('cathi_materializecss','https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css', [], '1.0','all');
    wp_enqueue_style('cathi_rellax',get_template_directory_uri().'/assets/js/rellax-master/css/main.css', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('cathi_owlcarousel',get_template_directory_uri().'/assets/js/owl.carousel2/dist/assets/owl.carousel.css', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('cathi_main_style',get_template_directory_uri().'/style.css?v=1.4', ['cathi_materializecss'], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('cathi_bootstrap',get_template_directory_uri().'/assets/css/grid-system.min.css?v=1.0', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('cathi_material','https://fonts.googleapis.com/icon?family=Material+Icons', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('cathi_aos',get_template_directory_uri().'/assets/js/aos/aos.css', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('cathi_fancybox',get_template_directory_uri().'/assets/js/jquery.fancybox/dist/jquery.fancybox.min.css', [], wp_get_theme()->get('version'),'all');
}

add_action('wp_enqueue_scripts','cathi_register_styles');



function cathi_register_scripts(){
    wp_enqueue_script('cathi_jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(),'3.5.1',true);
    wp_enqueue_script('cathi_materializecss_js','https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js', array(),'1.0.0',true);
    wp_enqueue_script('cathi_rellax_js',get_template_directory_uri().'/assets/js/rellax-master/rellax.min.js', array(),'1.0.0',true);
    wp_enqueue_script('cathi_owlcarousel_js',get_template_directory_uri().'/assets/js/owl.carousel2/dist/owl.carousel.min.js', array('cathi_jquery'),'1.0.0',true);
    wp_enqueue_script('cathi_aos',get_template_directory_uri().'/assets/js/aos/aos.js',array(),'1.0.0',true);
    wp_enqueue_script('cathi_init',get_template_directory_uri().'/assets/js/init.js?v=1.0.4',array('cathi_materializecss_js'),'1.0.2',true);
    wp_enqueue_script('cathi_parallax',get_template_directory_uri().'/assets/js/parallax-js/parallax.min.js',array(),'1.0.0',true);
    wp_enqueue_script('cathi_isotope',get_template_directory_uri().'/assets/js/isotope.min.js',array(),'1.0.0',true);
    wp_enqueue_script('cathi_fancybox',get_template_directory_uri().'/assets/js/jquery.fancybox/dist/jquery.fancybox.min.js',array(),'1.0.0',true);

}

add_action('wp_enqueue_scripts','cathi_register_scripts');

function extractYoutubeID($URL){
    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $URL, $matches);
    return $matches[0];
}


function extractQueruyValueFrmoURL($param, $URL){
    $parts = parse_url($URL);
    parse_str($parts['query'], $query);
    return $query[$param];
}



function show_page($id) {
    $post = get_post($id);
    $content = apply_filters('the_content', $post->post_content);
    echo $content;
}

function sendmail($subject,$to,$body){
    $content = '<html><head><title>'.$subject.'</title></head><body style="background-color:#ffffff;"><div align="center" style="margin-bottom:20px;"><img src="'.get_site_url().'/wp-content/themes/cathitemplate/assets/images/logo.png" width="200" border="0" style="margin:auto;" /></div><div style="margin:auto; background-color:rgba(0,0,0,0.05);  max-width:560px; width:100%; clear:both; padding:20px; font-family:Hind, sans-serif; font-size:14px; color:#000000; text-align:left; margin-bottom:20px; ">'.$body.'</div>';
    $content .= '</div></body></html>';

    date_default_timezone_set('Etc/UTC');

    require_once 'PHPMailer-master/PHPMailerAutoload.php';

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->SMTPSecure = "tls";
    $mail->Host = "smtp.ionos.de";
    $mail->Port = "587";
    $mail->SMTPAuth = true;
    $mail->Username = "apply@cathi-online.com";
    $mail->Password = "VCY4dmWrB=?(uyK";


    $mail->Subject = $subject;
    $mail->setFrom("apply@cathi-online.com", "CATHI");
    $mail->addAddress($to);

    if ($mail->addReplyTo($to, $to)) {
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        //Build a simple message body

        $mail->Body = $content;
        //Send the message, check for errors
        if(!$mail->Send()) {
            $arrResult = array ('response'=>'error');
        }

        $arrResult = array ('response'=>'success');
        return 1;
    } else {
        $arrResult = array ('response'=>'error');
        return 0;

    }
}

if( function_exists('acf_add_options_page') ) {

    $languages = array( 'en', 'es' );

    foreach ( $languages as $lang ) {
        acf_add_options_page( array(
            'page_title' => 'Site Options (' . strtoupper( $lang ) . ')',
            'menu_title' => __('Site Options (' . strtoupper( $lang ) . ')', 'text-domain'),
            'menu_slug'  => "site-options-${lang}",
            'post_id'    => $lang
        ) );
    }

}

//Susbcriptions Part

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Custom_Table_Example_List_Table extends WP_List_Table
{

    function column_default($item, $column_name)
    {
        return $item[$column_name];
    }

    function get_columns()
    {
        $columns = array(
            'email' => __('email', 'custom_table_example'),
        );
        return $columns;
    }

    function get_sortable_columns()
    {
        $sortable_columns = array(
            'email' => array('email', false),
        );
        return $sortable_columns;
    }

    function prepare_items()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'newsletter';

        $per_page = 20; // constant, how much records will be shown per page

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name WHERE cancelled=0");

        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged']) ? ($per_page * max(0, intval($_REQUEST['paged']) - 1)) : 0;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'email';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';

        $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name  WHERE cancelled=0 ORDER BY $orderby $order LIMIT %d OFFSET %d ", $per_page, $paged), ARRAY_A);

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }
}

function custom_table_example_admin_menu()
{
    add_menu_page(__('Subscriptions', 'custom_table_example'), __('Subscriptions', 'custom_table_example'), 'activate_plugins', 'Subscriptions', 'custom_table_example_persons_page_handler');
}

add_action('admin_menu', 'custom_table_example_admin_menu');


function custom_table_example_persons_page_handler()
{
    global $wpdb;

    $table = new Custom_Table_Example_List_Table();
    $table->prepare_items();

    $message = '';
    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'custom_table_example'), count($_REQUEST['id'])) . '</p></div>';
    }
    ?>
    <div class="wrap">

        <h2><?php _e('Subscriptions to Newsletter', 'custom_table_example')?></h2>
        <?php echo $message; ?>

        <form id="persons-table" method="GET">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
            <?php $table->display() ?>
        </form>

    </div>
    <?php
}
