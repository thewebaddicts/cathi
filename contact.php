<?php /* Template Name: Page | Contact us */ ?>

<?php get_header(); ?>
<?php

$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$meta  =get_fields($post_id);

$home_label= "Home";
$search_label= "Search...";


if(pll_current_language() == 'ar'){
    $home_label= "الصفحة الرئيسية";
    $search_label= "ابحث";
}elseif (pll_current_language() == 'de'){
    $home_label = "Startseite";
    $search_label = "Suche...";
}
?>

<div class="menu-spacer"></div>
<div class="contact-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $meta['image']; ?>"></div>
            <div class="banner-info col-12 col-md-10">
                <div class="banner-label"><?php echo  $meta['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $meta['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

    <div class="contact-us section-120-120 bg-03">
        <div class="section-inner contact-inner col-md-10 ">
            <div class="contact-info">
                    <div class="flex-row contact-item" data-aos="fade-up" data-aos-delay="100">
                        <i class="fas fa-envelope"></i>
                        <div class="row-info">
                            <div class="row-label"><?php if(isset($meta['email_label'])) echo $meta['email_label'] ?></div>
                            <div class="row-desc op-45"><?php echo $meta['contact_information']['email'] ?></div>
                        </div>
                    </div>
                    <div class="flex-row contact-item" data-aos="fade-up" data-aos-delay="200">
                        <i class="fas fa-phone-alt"></i>
                        <div class="row-info">
                            <div class="row-label"><?php if(isset( $meta['phone_number_label'])) echo $meta['phone_number_label'] ?></div>
                            <div class="row-desc op-45"><?php echo $meta['contact_information']['phone_number'] ?></div>
                        </div>
                    </div>
                    <div class="flex-row contact-item" data-aos="fade-up" data-aos-delay="300">
                        <i class="fas fa-fax"></i>
                        <div class="row-info">
                            <div class="row-label"><?php if(isset($meta['fax_label'])) echo $meta['fax_label'] ?></div>
                            <div class="row-desc op-45"><?php echo $meta['contact_information']['fax_number'] ?></div>
                        </div>
                    </div>
                    <div class="flex-row contact-item" data-aos="fade-up" data-aos-delay="400">
                        <i class="fas fa-location-arrow"></i>
                        <div class="row-info">
                            <div class="row-label"><?php if(isset($meta['address']))  echo $meta['address'] ?></div>
                            <div class="row-desc op-45"><?php echo $meta['contact_information']['address'] ?></div>
                        </div>
                    </div>
            </div>
            <div class="distributors flex-column">
                <div class="distributors-inner" data-aos="fade-up">
                    <div class="dist-info flex-column">
                        <div class="guideline"><?php if(isset($meta['find_nearest_label'])) echo $meta['find_nearest_label'] ?></div>
                        <div class="search-distributors">
                            <div role="search"  id="searchDistributors" class="searchDistributors">
                                <input type="text"  name="search_distributor" id="search_distributor" placeholder="<?php echo $search_label ?>" class="browser-default search_distributor" onkeyup="DataSearch()">
                                <div type="submit" id="search" onclick="DataSearch()">
                                    <div class="contain search-icon" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/search.png')"></div>
                                </div>
                            </div>
                            <?php if(pll_current_language() == 'en'){ ?>
                            <div class="results-dist"> <?php echo count( $meta['distributors']) ?> results found</div>
                            <?php } ?>
                        </div>
                        <div class="distributors-result">
                            <?php $i=0; foreach ($meta['distributors'] AS $distributor){ $i++; ?>
                                <div class="distributor-card" >
                                    <div class="general" onclick="if(!$(this).parent('.distributor-card').hasClass('active')){GetDirections('<?php echo $distributor['latitude']?>','<?php echo $distributor['longitude']?>');} $(this).parent('.distributor-card').toggleClass('active');">
                                        <div class="d-name"><?php echo $distributor['name'] ?></div>
                                        <div class="location">
                                            <div class="d-city"><?php echo $distributor['city'] ?>,</div>
                                            <div class="d-country"><?php echo $distributor['country'] ?></div>
                                        </div>
                                    </div>
                                    <div class="details">
                                        <div class="flex-row">
                                            <div class="row-label"><?php if(isset($meta['email_label'])) echo $meta['email_label'] ?></div>
                                            <div class="row-info"><?php echo $distributor['email'] ?></div>
                                        </div>
                                        <div class="flex-row">
                                            <div class="row-label"><?php if(isset( $meta['phone_number_label'])) echo $meta['phone_number_label'] ?></div>
                                            <div class="row-info"><?php echo $distributor['phone_number'] ?></div>
                                        </div>
                                        <div class="flex-row">
                                            <div class="row-label"><?php if(isset($meta['address']))  echo $meta['address'] ?></div>
                                            <div class="row-info"><?php echo $distributor['address'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="map">
                    
                    <iframe frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" scrolling="no" marginheight="0" marginwidth="0"  src="https://www.openstreetmap.org/export/embed.html"></iframe>
 
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="application section-80-80 bg-gray">
        <?php
        if(pll_current_language() == "ar")
            $academy = get_fields('1150');
        elseif (pll_current_language() == "de")
            $academy = get_fields('1148');
        else
            $academy= get_fields('64'); ?>

        <div class="section-inner application-inner col-md-10 col-lg-6 flex-column">
            <div class="form-label" data-aos="fade-up" data-aos-delay="100"><?php if(isset($academy['form_label']))  echo $academy['form_label'] ?></div>
            <div class="contact-form" data-aos="fade-up" data-aos-delay="200">
                <form  action="" method="POST" enctype="multipart/form-data" class="ContactForm">
                    <div class="user-info">
                        <div class="row-50">
                            <div class="input-label"><?php if(isset($academy['first_name_label']))  echo $academy['first_name_label'] ?></div>
                            <input type="text" name="first_name" placeholder="<?php if(isset($academy['first_name_label']))  echo $academy['first_name_label'] ?>" class="browser-default" required>
                        </div>
                        <div class="row-50">
                            <div class="input-label"><?php if(isset($academy['last_name_label']))  echo $academy['last_name_label'] ?></div>
                            <input type="text" name="last_name" placeholder="<?php if(isset($academy['last_name_label']))  echo $academy['last_name_label'] ?>" class="browser-default" required>
                        </div>
                        <div class="row-100">
                            <div class="input-label"><?php if(isset($academy['email_label']))  echo $academy['email_label'] ?></div>
                            <input type="email" name="email" placeholder="<?php if(isset($academy['email_label']))  echo $academy['email_label'] ?>" class="browser-default" required>
                        </div>
                        <div class="row-50">
                            <div class="input-label"><?php if(isset($academy['phone_number']))  echo $academy['phone_number'] ?></div>
                            <input type="text" name="phone_number" placeholder="<?php if(isset($academy['phone_number']))  echo $academy['phone_number'] ?>" class="browser-default" required>
                        </div>
                        <div class="row-50">
                            <div class="input-label"><?php if(isset($academy['country_label']))  echo $academy['country_label'] ?></div>
                            <select class="browser-default country-select"  name="country-select" required>
                                <option value="" selected disabled><?php if(isset($academy['country_label']))  echo $academy['country_label'] ?></option>
                                <?php
                                global $wpdb;
                                $result = $wpdb->get_results ( "SELECT * FROM countries" );
                                foreach ( $result as $country ) {
                                    ?>
                                    <option value="<?php echo $country->name ?>"><?php echo $country->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="row-100">
                            <div class="input-label"><?php if(isset($academy['message_label']))  echo $academy['message_label'] ?></div>
                            <textarea  name="message" placeholder="<?php if(isset($academy['message_label']))  echo $academy['message_label'] ?>" class="browser-default" required></textarea>
                        </div>
                        <div class="g-recaptcha" data-sitekey="6LeGXKwaAAAAAGF4O6ONFFhiilk5NDtHNsZvrMo5"></div>
                        <div class="app-submit">
                            <button type="submit" name="submit" class="c-button" data-aos="fade-up" data-aos-delay="300">
                                <div class="btn-text"><?php if(isset($academy['form_button_label']))  echo $academy['form_button_label'] ?></div>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>

<script type ='text/JavaScript'>
    <?php

    if(isset($_POST['submit'])){
        // Get the submitted form data
        $postData = $_POST;
        $name = $_POST['first_name'].' '.$_POST['last_name'];
        $email = $_POST['email'];
        $phone_number = $_POST['phone_number'];
        $country= $_POST['country-select'];
        $subject = 'New Contact Application';
        $message = $_POST['message'];

        if(!empty($_POST['g-recaptcha-response']))
        {
            $secret = '6LeGXKwaAAAAAMQzuSoPqh-i5OEtVGMZlz6eche9';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success){
                if(!empty($email) && !empty($name) && !empty($subject) && !empty($message) && !empty($phone_number) && !empty($country)){

                    // Validate email
                    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
                        echo 'Please enter your valid email.';
                    }else{

                        // Recipient
                        $toEmail = trim($meta['send_form_to_email']);

                        // Subject
                        $emailSubject = 'New Contact Application';

                        // Message
                        $htmlContent = '<h2>New Course Application</h2>
                                    <p><b style="color:black">Full Name:</b> '.$name.'</p>
                                    <p><b style="color:black">Email:</b> '.$email.'</p>
                                    <p><b style="color:black">Country:</b> '.$country.'</p>
                                    <p><b style="color:black">Phone Number:</b> '.$phone_number.'</p>
                                    <p><b style="color:black">Message:</b><br/>'.$message.'</p>';

                        $response = sendmail($emailSubject, $toEmail, $htmlContent);

                        if($response == 1){
                            echo "ShowMessage('Thank You' ,'Your Application has been submitted')";
                        }else if($response == 0 ){
                            echo "ShowMessage('Sorry' ,'Could not submit you application, try again later!')";
                        }

                    }
                }else{
                    echo "ShowMessage('Error' ,'please fill all fields')";
                }
            }else{
                echo "ShowMessage('Recaptcha' ,'Error in submitting Recaptcha')";
            }
        }else{
            echo "ShowMessage('Recaptcha' ,'Recaptcha field required')";
        }

    }
    ?>
    
    GetDirections(<?php echo $meta['contact_information']['latitude']?>, <?php echo $meta['contact_information']['longitude']?>);
    
</script>
