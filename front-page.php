<?php get_header(); ?>
<?php
$post_id = pll_get_post( get_the_ID(), pll_current_language() );
$Banner =get_fields($post_id);

$search_label= "Search...";

if(pll_current_language() == 'ar'){
    $search_label= "ابحث";
}elseif (pll_current_language() == 'de'){
    $search_label = "Suche...";
}
?>

<div class="menu-spacer"></div>
<div class="home-page ">

    <?php if($Banner){ ?>
    <div class="homepage-banner">
            <?php
            if(count($Banner['banner_slideshow'])>0){ ?>

                <div class="banner-carousel owl-carousel carousel" data-carousel-dotsdata="false" data-carousel-items="1" data-carousel-autoplay="false" data-carousel-loop="false" data-carousel-nav="false" data-carousel-dots="false" data-carousel-nav-triggers="false" data-carousel-nav-next-id="" data-carousel-nav-prev-id="" data-autoplay-timeout="10000">
                    <?php foreach ($Banner['banner_slideshow'] as $banner){ ?>
                        <div class="slide">
                            <div class="image cover" style="background-image: url('<?php echo $banner['image']; ?>')"></div>
                            <div class="banner-info col-10">
                                <div class="sublabel "><?php echo  $banner['sublabel']; ?></div>
                                <div class="label "><?php echo  $banner['label']; ?></div>
                                <div class="description "><?php echo  $banner['description']; ?></div>
                            </div>
                            <div class="gradient-round"></div>
                        </div>
                   <?php } ?>
                </div>
            <?php } ?>
    </div>
    <?php } ?>

    <div class="white-block display-block">
        <div class="display-block-inner col-md-10">
            <div class="block-label" data-aos="fade-up" data-aos-delay="100"><?php echo  $Banner['video_label']; ?></div>
            <div class="block-info">
                <div class="block-description" data-aos="fade-up" data-aos-delay="100"><?php echo  nl2br($Banner['video_description']); ?></div>
                <div class="block-visual" data-aos="fade-up" data-aos-delay="100">
                    <a href="https://www.youtube.com/embed/<?php echo  extractYoutubeID($Banner['video_url']) ?>" target="_BLANK"><img src="https://img.youtube.com/vi/<?php echo  extractYoutubeID($Banner['video_url']) ?>/0.jpg" /></a>
                </div>
            </div>
        </div>
    </div>

    <div class="home-products section-120-120">
        <div class="section-inner col-md-10 flex-column">
            <div class="section-label" data-aos="fade-up"><?php if(isset($Banner['products_section'])) echo $Banner['products_section'] ?></div>
            <div class="products-inner">
                <?php $i=0; foreach ($Banner['products_info'] AS $product){ $i++; ?>
                    <a href="<?php echo $product['page_link'] ?>" class="home-product-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                        <div class="page-image cover ratio-4-3" style="background-image: url('<?php echo  $product['image']; ?>')"></div>
                        <div class="page-label"><?php echo $product['label'] ?></div>
                        <div class="page-description op-45 t-lines-3"><?php echo $product['description'] ?></div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>


    <?php if($Banner['highlighted_testimonials']  != null){ ?>
    <div class="home-testimonials section-80-80">
        <div class="section-inner col-md-10 flex-column">
            <div class="section-label" data-aos="fade-up"><?php if(isset($Banner['testimonial_section'])) echo $Banner['testimonial_section'] ?></div>
            <div class="testimonials-row">
                <?php $i=0; foreach ($Banner['highlighted_testimonials'] AS $testimonial){ $i++; ?>
                    <?php
                        $testimonial_ID =url_to_postid( $testimonial );
                        $PostInfo = get_post($testimonial_ID);
                        $PostMeta = get_post_meta($testimonial_ID);
                    ?>

                    <div class="testimonial-card float-left" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                        <div class="quote" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/quotes.png')"></div>
                        <div class="testimnonial-info">
                            <div class="testimonial t-lines-8"><?php echo nl2br($PostInfo->testimonial) ?></div>
                            <div class="fullName"><?php echo $PostInfo->full_name ?></div>
                            <div class="position"><?php echo $PostInfo->position ?></div>
                            <?php $image_src = wp_get_attachment_image_src($PostInfo->testimonial_image,'large'); ?>

                            <a data-fancybox="images" href="<?php echo $image_src[0]  ?>"  class="item-card-container">
                                <div class="image"><?php echo wp_get_attachment_image( $PostInfo->testimonial_image);?></div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <?php
            if(pll_current_language() == "ar")
                $href= '/testimonial-page-arabic/';
            elseif (pll_current_language() == "de")
                $href= '/testimonial-page-deutsch/';
            else
                $href= '/testimonial-page/'; ?>

            <a href="<?php echo $href ?>" class="c-button testimonials-button"  data-aos="fade-up" data-aos-delay="200">
                <div class="btn-text"><?php if(isset($Banner['testimonial_section_button'])){ echo $Banner['testimonial_section_button'] ;}else{ echo 'View All' ;} ?></div>
            </a>
        </div>
    </div>
    <?php } ?>

    <div class="partners section-100-100">
        <div class="section-inner col-md-10 flex-column">
            <div class="section-label" data-aos="fade-up"><?php if(isset($Banner['partners_section'])) echo $Banner['partners_section'] ?></div>
            <div class="partners-row">
                <?php $i=0; foreach ($Banner['partners_logo'] AS $partner){ $i++; ?>
                    <div class="partner-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                        <div class="logo contain" style="background-image: url('<?php echo $partner['logo'] ?>')"></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="contact-us section-120-120">
        <?php
        if(pll_current_language() == "ar")
            $Contact = get_fields('1154');
        elseif (pll_current_language() == "de")
            $Contact = get_fields('1152');
        else
            $Contact= get_fields('70'); ?>

        <div class="section-inner  contact-inner col-md-10 ">
            <div class="contact-info flex-column">
                <div class="section-label" data-aos="fade-up" data-aos-delay="100"><?php echo $Contact['contact_information_section'] ?></div>
                <div class="contact-inner">
                    <div class="flex-row" data-aos="fade-up" data-aos-delay="200">
                        <div class="row-label"><?php if(isset($Contact['email_label'])) echo $Contact['email_label'] ?></div>
                        <div class="row-info"><?php echo $Contact['contact_information']['email'] ?></div>
                    </div>
                    <div class="flex-row" data-aos="fade-up" data-aos-delay="300">
                        <div class="row-label"><?php if(isset($Contact['phone_number_label'])) echo $Contact['phone_number_label'] ?></div>
                        <div class="row-info"><?php echo $Contact['contact_information']['phone_number'] ?></div>
                    </div>
                    <div class="flex-row" data-aos="fade-up" data-aos-delay="400">
                        <div class="row-label"><?php if(isset($Contact['address'])) echo $Contact['address'] ?></div>
                        <div class="row-info"><?php echo $Contact['contact_information']['address'] ?></div>
                    </div>
                    <div class="flex-row" data-aos="fade-up" data-aos-delay="500">
                        <div class="row-label"></div>
                        <a class="get-directions" onclick="GetDirections('<?php echo $Contact['contact_information']['latitude']?>','<?php echo $Contact['contact_information']['longitude']?>')"><?php if(isset($Contact['get_directions_label'])) echo $Contact['get_directions_label'] ?></a>
                    </div>

                </div>
            </div>
            <div class="distributors flex-column">
                <div class="section-label" data-aos="fade-up" data-aos-delay="100"><?php if(isset($Contact['distributors_section'])) echo $Contact['distributors_section'] ?></div>
                <div class="distributors-inner" data-aos="fade-up" data-aos-delay="200">
                    <div class="dist-info flex-column">
                        <div class="guideline"><?php if(isset($Contact['find_nearest_label'])) echo $Contact['find_nearest_label'] ?></div>
                        <div class="search-distributors">
                            <div role="search"  id="searchDistributors" class="searchDistributors">
                                <input type="text"  name="search_distributor" id="search_distributor" placeholder="<?php echo $search_label ?>" class="browser-default search_distributor" onkeyup="DataSearch()">
                                <div type="submit" id="search" onclick="DataSearch()">
                                    <div class="contain search-icon" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/search.png')"></div>
                                </div>
                            </div>
                            <?php if(pll_current_language() == 'en'){ ?>
                            <div class="results-dist"> <?php echo count( $Contact['distributors']) ?> results found</div>
                            <?php } ?>
                        </div>
                        <div class="distributors-result">
                            <?php $i=0; foreach ($Contact['distributors'] AS $distributor){ $i++; ?>
                                <div class="distributor-card">
                                    <div class="general"  onclick="if(!$(this).parent('.distributor-card').hasClass('active')){GetDirections('<?php echo $distributor['latitude']?>','<?php echo $distributor['longitude']?>');} $(this).parent('.distributor-card').toggleClass('active');">
                                        <div class="d-name"><?php echo $distributor['name'] ?></div>
                                        <div class="location">
                                            <div class="d-city"><?php echo $distributor['city'] ?>,</div>
                                            <div class="d-country"><?php echo $distributor['country'] ?></div>
                                        </div>
                                    </div>
                                    <div class="details">
                                        <div class="flex-row">
                                            <div class="row-label"><?php if(isset($Contact['email_label'])) echo $Contact['email_label'] ?></div>
                                            <div class="row-info"><?php echo $distributor['email'] ?></div>
                                        </div>
                                        <div class="flex-row">
                                            <div class="row-label"><?php if(isset($Contact['phone_number_label'])) echo $Contact['phone_number_label'] ?></div>
                                            <div class="row-info"><?php echo $distributor['phone_number'] ?></div>
                                        </div>
                                        <div class="flex-row">
                                            <div class="row-label"><?php if(isset($Contact['address'])) echo $Contact['address'] ?></div>
                                            <div class="row-info"><?php echo $distributor['address'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="map">
                       <iframe frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" src="https://www.openstreetmap.org/export/embed.html"></iframe>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="awards section-50-50">
        <div class="section-inner col-md-10 flex-column">
        <div class="section-label" data-aos="fade-up"><?php if(isset($Banner['awards_section'])) echo $Banner['awards_section'] ?></div>
            <div class="awards-row">
                
                    <div class="award-card" data-aos="fade-up" data-aos-delay="300">
                        <div class="logo contain" style="background-image: url('<?php echo $Banner['dgsim_image'] ?>')"></div>
                    </div>
                    
                     <div class="award-card" data-aos="fade-up" data-aos-delay="350">
                        <div class="logo contain" style="background-image: url('<?php echo $Banner['itr_image'] ?>')"></div>
                    </div>
                
            </div>
        </div>
    </div>

</div>

<?php get_footer(); ?>

<script type ='text/JavaScript'>
    GetDirections('<?php echo $Contact['contact_information']['latitude']?>','<?php echo $Contact['contact_information']['longitude']?>')
</script>
