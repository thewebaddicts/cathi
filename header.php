<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
$url = $_SERVER['REQUEST_URI'];
$urlParts = explode ('/', $url);
$language = $urlParts[1] ;
$lang = "en";
$search_label= "Search...";

if($language == 'ar')
    $lang='ar';

if(pll_current_language() == 'ar'){
    $search_label= "ابحث...";
}elseif (pll_current_language() == 'de'){
    $search_label = "Suche...";
}
?>

<html lang="<?php echo $lang ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body>
<?php $TopInfo = get_fields('5'); ?>
<header class="main_menu">
    <div class="top">
        <div class="content-header">
            <div class="logo padding-right-60 logo-mobile">
                <?php if(function_exists('the_custom_logo')){ the_custom_logo(); } ?>
            </div>
            <div class="features">
                <div class="search border-left">
                    <form role="search" action="/search-cathi" method="get" id="searchform" class="searchform">
                        <input type="text"  name="search" id="search" placeholder="<?php echo $search_label ?>" class="browser-default input-search-header">
                        <button type="submit" id="searchsubmit">
                            <div class="contain search-icon" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/search.png')"></div>
                        </button>
                    </form>
                    <a href="/search-cathi" class="contain search-icon-mobile" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/search.png')"></a>
                </div>
                <div class="languages border-left mobile-no-border">
                    <?php
                    wp_nav_menu([
                        "menu" => "primary",
                        "theme_location" => "language",
                        "container" => "",
                        "items_wrao" => "<ul></ul>",
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <nav>
        <div class="content-header">
            <a href="/search-cathi" class="search-mobile pos-left-0"><i class="fas fa-search"></i></a>
            <a href="#" class="toggle pos-right-0"><i class="fas fa-bars mobile-burgers"></i><i class="fas fa-times"></i></a>
            <?php
            wp_nav_menu([
                "menu" => "primary",
                "theme_location" => "primary",
                "container" => "",
                "items_wrao" => "<ul></ul>",
            ]);
            wp_nav_menu([
                "menu" => "mobile",
                "theme_location" => "mobile",
                "container" => "",
                "items_wrao" => "<ul></ul>",
                "menu_class" => "mobile"
            ]);
            ?>
        </div>
    </nav>

</header>

<?php if(is_admin_bar_showing()){ ?>
    <style>
        header.main_menu{
            top:30px !important;
        }

        @media screen  and (max-width: 767px){
            header.main_menu{
                top:45px !important;
            }

            header.compact{
                top: 0 !important;
            }
        }
    </style>
<?php } ?>
