<div class="footer-menu">
    <div class="top">
        <div class="policies">
            <?php wp_nav_menu([
                "menu" => "footer",
                "theme_location" => "footer",
                "container" => "",
                "items_wrao" => "<ul></ul>",
                "depth" => 1,
            ]); ?>
        </div>
        <?php $TopInfo = get_fields('5'); ?>
        
        <div class="newsletter  border-right">
        </div>
		
        <div class="social-icons  border-right">
            <?php if(array_key_exists("facebook_link",$TopInfo) && array_key_exists("url",$TopInfo['facebook_link']) && $TopInfo['facebook_link']['url']!=''){ ?><a href="<?php echo $TopInfo['facebook_link']['url'] ?>" target="_blank" class="social" style="margin-left:-10px;"><?php echo htmlspecialchars_decode($TopInfo['facebook_link']['title']); ?></a><?php } ?>
            <?php if(array_key_exists("twitter_link",$TopInfo) && array_key_exists("url",$TopInfo['twitter_link']) && $TopInfo['twitter_link']!=''){ ?><a href="<?php echo $TopInfo['twitter_link']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($TopInfo['twitter_link']['title']); ?></a><?php } ?>
            <?php if(array_key_exists("youtube_link",$TopInfo) && array_key_exists("url",$TopInfo['youtube_link']) && $TopInfo['youtube_link']!=''){ ?><a href="<?php echo $TopInfo['youtube_link']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($TopInfo['youtube_link']['title']); ?></a><?php } ?>
            <?php if(array_key_exists("linkedin_link",$TopInfo) && array_key_exists("url",$TopInfo['linkedin_link']) && $TopInfo['linkedin_link']!=''){ ?><a href="<?php echo $TopInfo['linkedin_link']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($TopInfo['linkedin_link']['title']); ?></a><?php } ?>
        </div>
		
        <div class="languages mobile-languages">
            <?php
            wp_nav_menu([
                "menu" => "primary",
                "theme_location" => "language",
                "container" => "",
                "items_wrao" => "<ul></ul>",
            ]);
            ?>
        </div>

    </div>
    <div class="bottom">
        <div class="col s12">Copyright © 2003 - <?php echo date("Y"); ?> CATHI GmbH. All rights reserved.Designed and Developed by <a href="https://thewebaddicts.com" target="_blank" class="developed">The Web Addicts</a></div>
    </div>
</div>

<?php
wp_footer();
?>

<script type ='text/JavaScript'>

    <?php
    if(isset($_POST['submitNews'])) {
        // Get the submitted form data

        global $wpdb;
        $table = $wpdb->prefix.'newsletter';
        $email = $_POST['news_email'];

        // Check whether submitted data is not empty
        if (!empty($email)) {

            // Validate email
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                echo 'Please enter your valid email.';
            } else {

                $checkIfExists = $wpdb->get_var("SELECT ID FROM $table WHERE email = '$email' AND cancelled =0");

                if ($checkIfExists == NULL) {

                    $data = array(
                        'cancelled' => 0,
                        'email' => $email,
                    );
                    $format = array(
                        '%s',
                        '%s'
                    );
                    $success = $wpdb->insert($table, $data, $format);
                    if ($success) {
                        $meta = get_fields('5');
                        // Recipient
                        $toEmail = trim($meta['send_newsletter_form_to_email']);

                        // Subject
                        $emailSubject = 'Newsletter Subscription';

                        // Message
                        $htmlContent = '<h2>New Newsletter Subscription</h2>
                                    <p><b style="color:black">Email:</b> '. $email.'</p>';

                        $response = sendmail($emailSubject, $toEmail, $htmlContent);
                        if($response == 1){
                            echo "ShowMessage('Thank You' ,'Your Subscription has been confirmed')";
                        }else if($response == 0 ){
                            echo "ShowMessage('Sorry' ,'Your are already subscribed to our newsletter')";
                        }
                    }
                }else{
                    echo "ShowMessage('Sorry' ,'Your are already subscribed to our newsletter')";
                }

            }
        }
    }
    ?>

</script>

<script src='https://www.google.com/recaptcha/api.js' async defer ></script>

</body>
</html>
