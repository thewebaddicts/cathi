<?php /* Template Name: Template | News & Media */ ?>

<?php get_header(); ?>
<?php

$meta = get_fields(get_the_ID());
$home_label= "Home";

if(pll_current_language() == "ar"){
    $banner = get_fields('1166');
    $home_label= "الصفحة الرئيسية";
} elseif (pll_current_language() == "de"){
    $banner = get_fields('1164');
    $home_label = "Startseite";
} else{
    $banner = get_fields('68');
}


?>

<div class="menu-spacer"></div>
<div class="news-details-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $banner['image']; ?>" ></div>
            <div class="banner-info col-12 col-md-10">
                <div class="banner-label"><?php echo  $banner['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $banner['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

    <div class="details section-80-80">
        <div class="section-inner news-inner col-md-10 col-lg-7 flex-column">

            <div class="news-label" data-aos="fade-up" data-aos-delay="100"><?php echo $meta['label'] ?></div>

            <div class="news-sublabel" data-aos="fade-up" data-aos-delay="200"><?php echo $meta['sublabel'] ?></div>

            <div class="news-header">
                <div class="header-left" >
                    <?php $category = get_post($meta['category']);
                    if($category){
                        ?>
                        <div class="news-category margin-right-10" data-aos="fade-up" data-aos-delay="300"><?php echo $category->label ?></div>

                    <?php } ?>
                    <div class="news-date" data-aos="fade-up" data-aos-delay="400"><?php echo $meta['date'] ?></div>
                </div>
                <div class="header-right" data-aos="fade-up" data-aos-delay="500">
                   
                </div>
            </div>

            <div class="image ratio-5-3 cover" style="background-image: url('<?php echo $meta['image'] ?>')" data-aos="fade-up" data-aos-delay="300"></div>

            <?php $i=0; foreach ($meta['description'] AS $paragraph){ $i++; ?>
                <div class="description-row flex-column">
                    <div class="label" data-aos="fade-up" data-aos-delay="100"><?php echo $paragraph['title'] ?></div>
                    <div class="description" data-aos="fade-up" data-aos-delay="200"><?php echo nl2br($paragraph['description']) ?></div>
                </div>
            <?php } ?>

            <div class="news-bottom">
                <div class="border-line pos-left-45" data-aos="fade-up"></div>
                <a href="/news" class="c-button" data-aos="fade-up" data-aos-delay="100">
                    <span class="btn-text"><?php if(isset($banner['article_page_back_to_news'])) echo $banner['article_page_back_to_news'] ?></span>
                </a>


                    <?php
                    $pages = get_pages( array(
                        'child_of'    => 68,
                    ) );
                    if($pages) {
                        $pagesID = array();
                        foreach ($pages as $page) {
                            array_push($pagesID, $page->ID);
                        }

                        foreach($pagesID as $key => $pageID) {
                           if($pageID == get_the_ID()){
                               if (isset($pagesID[$key+1])) {
                                   $nextPageID =  $pagesID[$key+1]; // next element
                               } else {
                                   $nextPageID = $pagesID[0];
                               }
                           }
                        }
                    }
                    $next = get_fields($nextPageID);
                    ?>

                    <a href="<?php echo get_permalink($nextPageID) ?>" class="nextNews" data-aos="fade-up" data-aos-delay="200">
                        <div class="image-container borders-left">
                            <div class="next-image ratio-1-1 cover" style="background-image: url('<?php echo $next['image'] ?>')"></div>
                        </div>
                        <div class="information borders-right">
                            <div class="guideline"><?php if(isset($banner['article_page_view_next'])) echo $banner['article_page_view_next'] ?>
                                <?php $category = get_post($meta['category']);
                                if($category){
                                    ?>
                                    <?php echo $category->label ?>

                                <?php } ?>
                            </div>
                            <div class="next-label t-lines-3"><?php echo $next['label'];?></div>
                            <div class="next-button contain" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/next.svg')"></div>
                        </div>
                    </a>

            </div>
        </div>
    </div>

</div>


<?php get_footer(); ?>
