<?php /* Template Name: Template | Software */ ?>
<?php get_header(); ?>
<?php
$meta = get_fields(get_the_ID());

$parent_page = get_fields('60');
$parent_link = get_page_link( '60');

$home_label= "Home";



if(pll_current_language() == 'ar'){
    $home_label= "الصفحة الرئيسية";
    $parent_page = get_fields('1160');
    $parent_link = get_page_link( '1160');
}elseif (pll_current_language() == 'de'){
    $home_label = "Startseite";
    $parent_page = get_fields('1162');
    $parent_link = get_page_link( '1162');
}

?>

    <div class="menu-spacer"></div>
    <div class="product-details-page page-top">

        <div class="product-breadcrumbs">
            <div class="bread-inner col-md-10 section-inner">
                <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                <label class="py-2"> / </label>
                <a href="<?php echo  $parent_link?>" class="list-item py-2"><?php echo $parent_page['label'] ?></a>
                <label class="py-2"> / </label>
				<label class="py-2"><?php echo $meta['sublabel'] ?></label>
				<label class="py-2"> / </label>
                <label class="py-2"><?php echo $meta['label'] ?></label>
            </div>
        </div>

        <div class="details section-50-50 bg-03">
            <div class="section-inner col-md-10 flex-column">
                <?php  if(count($meta['information'])>0){?>
                    <div class="product-visual">
                        <div class="visual-images">
                            <div class="images-row margin-right-3"  data-aos="fade-up" data-aos-delay="200">
                                <?php $i=0; foreach ($meta['information'] AS $detail){ $i++; ?>
                                    <div id="sm-img_<?php echo $i ?>" class="sm-img ratio-1-1 cover <?php if($i==1) echo 'active' ?>" style="background-image: url('<?php echo $detail['image'] ?>')" onclick="changeDetail('<?php echo $i ?>')"></div>
                                <?php } ?>
                            </div>
                            <div class="md-images ratio-5-3" data-aos="fade-up">
                                <?php $i=0; foreach ($meta['information'] AS $detail){ $i++; ?>
                                    <a data-fancybox="images" href="<?php echo $detail['image']  ?>"  class="item-card-container">
                                        <div id="md-img_<?php echo $i ?>" class="md-img cover <?php if($i==1) echo 'active' ?>" style="background-image: url('<?php echo $detail['image'] ?>')"></div>
                                    </a>
                                <?php } ?>
                            </div>

                        </div>
                        <div class="visual-desc">
                            <div class="product-name"><?php echo $meta['label'] ?></div>
                            <?php $i=0; foreach ($meta['information'] AS $detail){ $i++; ?>
                                <div id="detail-card_<?php echo $i ?>" class="detail-card <?php if($i==1) echo 'active' ?>">
                                    <div class="description" data-aos="fade-up"><?php echo nl2br($detail['description']) ?></div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

                <?php  if(is_array($meta['descriptions']) && count($meta['descriptions'])>0){?>
                    <div class="product-descriptions">
                        <div class="labels-row"  data-aos="fade-up" data-aos-delay="200>">
                            <?php $i=0; foreach ($meta['descriptions'] AS $desc){ $i++; ?>
                                <div id="labelTag_<?php echo $i ?>" class="label-tag margin-right-25 <?php if($i==1) echo 'active' ?>" onclick="changeDescription('<?php echo $i ?>')"><?php echo $desc['title'] ?></div>
                            <?php } ?>
                        </div>

                        <div class="desc-row">
                            <?php $i=0; foreach ($meta['descriptions'] AS $desc){ $i++; ?>
                                <div id="desc_<?php echo $i ?>" class="desc <?php if($i ==1) echo 'active' ?>"><?php echo nl2br($desc['description']) ?></div>
                            <?php } ?>
                        </div>

                    </div>
                <?php } ?>

            </div>
        </div>

    </div>


<?php get_footer(); ?>