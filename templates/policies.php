<?php /* Template Name: Template | Policies */ ?>

<?php get_header(); ?>
<?php $meta = get_fields(get_the_ID());

if(pll_current_language() == "ar"){
    $home_label= "الصفحة الرئيسية";
}elseif (pll_current_language() == "de"){
    $home_label = "Startseite";
}else{
    $home_label= "Home";
}
?>

    <div class="menu-spacer"></div>
    <div class="policy-page">
        <div class="page-banner">
            <div class="banner-inner">
                <div class="image cover parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $meta['image']; ?>" ></div>
                <div class="banner-info col-12 col-md-10">
                    <div class="banner-label"><?php echo  $meta['policy_label']; ?></div>
                    <div class="breadcrumbs">
                        <div class="bread-inner">
                            <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                            <label class="py-2"> / </label>
                            <label class="py-2"><?php echo  $meta['policy_label']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="gradient-round"></div>
            </div>
        </div>

        <div class="section-100-100">
            <div class="section-inner col-md-10 flex-column">
                <div class="section-label" data-aos="fade-up" data-aos-delay="100"><?php echo $meta['policy_label'] ?></div>
                <div class="policies-inner" data-aos="fade-up" data-aos-delay="200">
                    <?php echo nl2br($meta['policy_description']) ?>
                </div>
            </div>
        </div>

    </div>

<?php get_footer(); ?>