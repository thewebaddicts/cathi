<?php /* Template Name: Template | Media */ ?>

<?php get_header(); ?>
<?php
$meta = get_fields(get_the_ID());

$home_label= "Home";

if(pll_current_language() == "ar"){
    $banner = get_fields('1166');
    $home_label= "الصفحة الرئيسية";
} elseif (pll_current_language() == "de"){
    $banner = get_fields('1164');
    $home_label = "Startseite";
} else{
    $banner = get_fields('68');
}

?>

<div class="menu-spacer"></div>
<div class="news-details-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $banner['image']; ?>" ></div>
            <div class="banner-info col-12 col-md-10">
                <div class="banner-label"><?php echo  $banner['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $banner['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

    <div class="details section-80-80">
        <div class="section-inner news-inner col-md-10 col-lg-7 flex-column">

            <div class="news-label" data-aos="fade-up" data-aos-delay="100"><?php echo $meta['label'] ?></div>

            <div class="news-sublabel" data-aos="fade-up" data-aos-delay="200"><?php echo $meta['sublabel'] ?></div>

            <div class="news-header">
                <div class="header-left" >
                    <?php $category = get_post($meta['category']);
                    if($category){
                        ?>
                        <div class="news-category" data-aos="fade-up" data-aos-delay="300"><?php echo $category->label ?></div>

                    <?php } ?>
                </div>
                <div class="header-right" data-aos="fade-up" data-aos-delay="500">
                    
                </div>
            </div>

            <div class="media-row">
                <?php $i=0; foreach ($meta['medias'] AS $media){ $i++;
                    ?>
                        <div class="media-container">
                            <div class="media-card ratio-1-1">
                                <a data-fancybox="images" href="<?php echo $media['image']  ?>"  class="item-card-container">
                                    <div class="image cover" style="background-image: url('<?php echo $media['image'] ?>')"></div>
                                </a>
                            </div>
                        </div>

                <?php } ?>
            </div>

            <div class="news-bottom">
                <div class="border-line"></div>
                <a href="/news" class="c-button" data-aos="fade-up" data-aos-delay="100">
                    <span class="btn-text"><?php if(isset($banner['article_page_back_to_news'])) echo $banner['article_page_back_to_news'] ?></span>
                </a>
            </div>
        </div>
    </div>

</div>


<?php get_footer(); ?>
