<?php /* Template Name: Page | News & Media */ ?>

<?php get_header(); ?>
<?php

    $post_id = pll_get_post( get_the_ID(), pll_current_language() );
    $meta =get_fields($post_id);

    $home_label= "Home";
    $search_label= "Search...";
    $lang_label= "en";
    $lang_ext = "" ;


    if(pll_current_language() == 'ar'){
        $lang_label = pll_current_language();
        $lang_ext = "_ar";
        $home_label= "الصفحة الرئيسية";
        $search_label= "ابحث";
    }elseif (pll_current_language() == 'de'){
        $lang_label = pll_current_language();
        $lang_ext = "_de";
        $home_label = "Startseite";
        $search_label = "Suche...";
    }



    if(isset($_GET['category'])){
        $filterCategory = $_GET['category'];
    }else{
        $filterCategory = "";
    }

    $pages = get_pages( array(
        'child_of'    => $post_id,
        'meta_key' => 'category',
        'meta_value' => $filterCategory,
    ));



        if(isset($_GET['date']) && $_GET['date']=="DESC" ){
            usort($pages, function($a, $b) {
                return strtotime($a->date) - strtotime($b->date);
            });
        }else{
            usort($pages, function($a, $b) {
                return strtotime($b->date) - strtotime($a->date);
            });
        }




?>

<div class="menu-spacer"></div>
<div class="news-page">
    <div class="page-banner">
        <div class="banner-inner">
            <div class="image parallax-window" data-position="left" data-parallax="scroll" data-image-src="<?php echo $meta['image']; ?>"></div>
            <div class="banner-info col-12 col-md-10">
                <div class="banner-label"><?php echo  $meta['label']; ?></div>
                <div class="breadcrumbs">
                    <div class="bread-inner">
                        <a href="<?php echo get_home_url(); ?>" class="list-item py-2"><?php echo $home_label ?></a>
                        <label class="py-2"> / </label>
                        <label class="py-2"><?php echo  $meta['label']; ?></label>
                    </div>
                </div>
            </div>
            <div class="gradient-round"></div>
        </div>
    </div>

    <a name="section1" class="a-name"></a>
    <div class="Filters">
        <div class="section-inner col-md-10">
            <div class=" date-filter filter-inner">
                <select class="browser-default" onchange="changeQueryParameter('date',$(this).val(),'section1')">
                    <option value="" disabled <?php if (!isset($_GET["date"])){?> selected  <?php  } ?>><?php if(isset($meta['filter_by_date_label'])) echo $meta['filter_by_date_label'] ?></option>
                    <option value="DESC" <?php if (isset($_GET["date"]) && $_GET["date"] == "DESC"){?> selected  <?php  } ?>><?php if(isset($meta['oldest_to_newest'])) echo $meta['oldest_to_newest'] ?></option>
                    <option value="ASC" <?php if (isset($_GET["date"]) && $_GET["date"] == "ASC"){?> selected  <?php  } ?>><?php if(isset($meta['newest_to_oldest'])) echo $meta['newest_to_oldest'] ?></option>
                </select>
            </div>

            <div class=" category-filter filter-inner padding-left-50">
                <select class="browser-default" onchange="changeQueryParameter('category',$(this).val(),'section1')">
                    <option value="" disabled <?php if (!isset($_GET["category"])){?> selected  <?php  } ?>><?php if(isset($meta['filter_by_category_label'])) echo $meta['filter_by_category_label'] ?></option>
                    <?php
                    $categories =get_posts( array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'category_name' => 'news_category'.$lang_ext,
                        'posts_per_page'=>-1,
                        'lang'=>$lang_label
                    ));

                    if($categories){
                        foreach ($categories as $category){
                            ?>
                            <option value="<?php echo $category->ID ?>" <?php if (isset($_GET["category"]) && $_GET["category"] == $category->ID){?> selected  <?php  } ?>><?php echo $category->label ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <div role="search"  id="searchBlock" class="searchBlock padding-left-50 mobile-padding-0">
                <div type="submit" id="search_news_btn" onclick="NewsSearch()">
                    <div class="contain search-icon margin-right-10" style="background-image: url('/wp-content/themes/cathitemplate/assets/images/Icons/search.png')"></div>
                </div>
                <input type="text"  name="search_input" id="search_input" placeholder="<?php echo $search_label ?>" class="browser-default search_input search_news" onkeyup="NewsSearch()">
            </div>

        </div>
    </div>

    <div class="news-media section-80-80">
        <div class="section-inner col-md-10 flex-column">
            <div class="news-row">
                <?php
                if($pages){
                    $i=0;
                    foreach ($pages as $page){
                        $i++;
                        ?>
                        <a href="<?php echo get_permalink( $page->ID )?>" class="news-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                            <div class="visual">
                                <?php $image_src = wp_get_attachment_image_src($page->image,'large'); ?>
                                <div class="image ratio-6-4 cover" style="background-image: url('<?php echo $image_src[0]  ?>')"></div>
                                <div class="gradient-15-60"></div>
                                <?php $category = get_post($page->category);
                                if($category){
                                ?>
                                <div class="category pos-left-22">
                                     <?php echo $category->label ?>
                                </div>
                                <?php } ?>
                                <div class="label pos-left-22"><?php echo $page->label ?></div>
                            </div>
                            <div class="description t-lines-3"><?php echo $page->sublabel ?></div>
                            <div class="c-button">
                                <?php $template_name = get_page_template_slug( $page->ID);
                                if($template_name == "templates/media.php"){
                                ?>
                                <div class="btn-text"><?php if(isset($meta['view_album_button_label'])) echo $meta['view_album_button_label'] ?></div>
                                <?php }else{ ?>
                                    <div class="btn-text"><?php if(isset($meta['read_more_button_label'])) echo $meta['read_more_button_label'] ?></div>
                                <?php } ?>
                            </div>
                        </a>
                    <?php
                    }}
                ?>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
